<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Freelance Engine
 * @since Freelance Engine 1.0
 */

get_header(); ?>

<section class="blog-header-container">
	<div class="container">
		<!-- blog header -->
		<div class="row">
		    <div class="col-md-12 blog-classic-top">
		        <h2><?php _e("Not Found", ET_DOMAIN); ?></h2>
		    </div>
		</div>
		<!--// blog header  -->
	</div>
</section>

<div class="container" style="background-color: white"; >
	<!-- block control  -->
	<div class="row block-posts block-page" style="background-image: url(https://www.brainhire.com/wp-content/uploads/2016/10/Page-Background.jpg); height:700px"; >
		<div class="col-md-8 col-sm-12 col-ms-12 posts-container" id="left_content">
            <div class="page-notfound-content" style="background-color: transparent";>
<p><h1><font color="#067b75"><?php _e( '404: Page not found.', ET_DOMAIN ); ?></font></h1></p>                
<p><h2><?php _e( 'We have looked practically everywhere', ET_DOMAIN ); ?></h2></p>
<?php _e( 'Double check the URL or head back to the homepage. If you continue to get this page, email us at', ET_DOMAIN ); ?>	
<font color="#067b75"><b><?php _e( 'support@skillquo.com', ET_DOMAIN ); ?></b></font>			
            </div><!-- end page content -->
		</div><!-- LEFT CONTENT -->
	
	</div>
	<!--// block control  -->
</div>


<?php

get_footer();
