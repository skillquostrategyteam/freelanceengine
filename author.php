<?php
/**
 * The Template for displaying a user profile
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */
global $wp_query, $ae_post_factory, $post, $user_ID, $wp_better_emails;
$post_object = $ae_post_factory->get(PROFILE);
$author_id 	= 	get_query_var( 'author' );
$author_name = get_the_author_meta('display_name', $author_id);
$author_available = get_user_meta($author_id, 'user_available', true);
// get user profile id
$profile_id = get_user_meta($author_id, 'user_profile_id', true);
// get post profile
$profile = get_post($profile_id);
$convert = '';

if( $profile && !is_wp_error($profile) ){
    $convert = $post_object->convert( $profile );
}

// try to check and add profile up current user dont have profile
if(!$convert && ( fre_share_role() || ae_user_role($author_id) == FREELANCER) ) {
    $profile_post = get_posts(array('post_type' => PROFILE,'author' => $author_id));
    if(!empty($profile_post)) {
        $profile_post = $profile_post[0];
        $convert = $post_object->convert( $profile_post );
        $profile_id = $convert->ID;
        update_user_meta($author_id, 'user_profile_id', $profile_id);
    }else {
        $convert = $post_object->insert( array( 'post_status' => 'publish' ,
                                                'post_author' => $author_id ,
                                                'post_title' => $author_name ,
                                                'post_content' => '')
                                        );

        $convert = $post_object->convert( get_post($convert->ID) );
        $profile_id = $convert->ID;
    }


}

//  count author review number
$count_review = fre_count_reviews($author_id);
// $count_project = fre_count_user_posts_by_type($user_ID, PROJECT, 'publish');

if(!$profile_id) {
	$profile_id = $author_id;
	$noProfile = true;
}

if($profile_id) {

	if($_POST["skillquo_approve"] && current_user_can( "activate_plugins" )) {
		$value = 0;
		$rejected_email = get_the_author_meta( 'user_email', $author_id);
		if($_POST["skillquo_approve"] == 1) { //Approval 
			$value = 1;
			wp_mail($rejected_email,
				"Your Profile has been approved!",
				$wp_better_emails->process_email_html("Congratulations! Your profile has been approved, and we are delighted to welcome you to our SkillQuo community. We wish you a fruitful partnership with SkillQuo. We take great care and pride in being a true community rather than - just a platform."."\r\n"."\r\n"."We encourage you to take advantage of the many resources at your disposal, from our blog site, our weekly subscription, and stay tuned for our most exciting reveal of all: SkillQuo Cares forum. This site will serve our comprehensive support community for our SkillQuo consultants."."\r\n"."\r\n"."We will provide everything from chat rooms, to white papers to featured consultant success stories. We will update you on our offsite and onsite activities such as meet-and-greets to sweepstakes and contests. As we said, we stand by our word to you in being a true community."."\r\n"."\r\n"."With SkillQuo, freelancing is associated with exclusivity, prestige, and the brightest minds solving the most intriguing, layered business problems."."\r\n"."\r\n"."We hope you feel empowered to chart your own path, augment your income, and build your powerhouse brand."."\r\n"."\r\n"."Please feel free to reach out to us at anytime, with questions."."\r\n"."\r\n"."Sincerely,"."\r\n"."Arathi Dar, Co-Founder & CMO at SkillQuo")
			);
		} else if($_POST["skillquo_approve"] == 2) { //Reject
			$value = 2; //Reject Once
			
			if(!$noProfile)
				$under_review = get_post_meta($profile_id,"under_review",true);
			else 
				$under_review = get_user_meta($profile_id,"under_review",true);
			
			if($under_review == 2) {
				$value = 3; //Perma Reject
			}
		
			$content .= "Thank you for taking the time to create a profile with SkillQuo.  Regretfully, your profile did not meet SkillQuo's requirements, and we are currently unable to approve your candidacy.  If you can take the following steps to update and complete your profile, we will re-review for acceptance:"."\r\n"."\r\n";
			if($_POST["reject_1"]) $content .= "- Profile Image not up to standard - Please carefully consider your profile image quality, professional and authentic content, along with spelling and clarity. "."\r\n"."\r\n";
			if($_POST["reject_2"]) $content .= "- Inappropriate Content"."\r\n"."\r\n";
			if($_POST["reject_3"]) $content .= "- Incomplete Profile - Your profile was incomplete with regard to employment history and education credentialing. Please view our exemplar profile to understand what we require in our SkillQuo Consultants."."\r\n"."\r\n";					
			if($_POST["reject_4"]) $content .= "- Insufficient Content"."\r\n"."\r\n";
			if($_POST["reject_5"]) $content .= "- Fake or Copied Profile"."\r\n"."\r\n";
			if($_POST["reject_6"]) $content .= "- Others - See reason below:"."\r\n";

			$content .= sprintf("%s",$_POST["reason"])."\r\n"."\r\n";
						
			$content .= "If you have any questions, please do reach out to us for more information, or go to your dashboard to edit your profile and post it again. We are glad to reconsider your updated profile, and wish you the best in your future endeavors."."\r\n"."\r\n"."Sincerely,"."\r\n"."SkillQuo Team"."\r\n"."\r\n";
									
			wp_mail($rejected_email,"Your Profile has been Rejected",$wp_better_emails->process_email_html($content));
		} else {
			wp_mail(
				$rejected_email,
				"Your Profile is being reviewed!",
				$wp_better_emails->process_email_html("Thank you for submitting your profile! Your profile has been received and our team is in the process of reviewing it for completeness, accuracy and fit for SkillQuo. We typically approve profiles within 4 business days, but sometimes it takes longer due to additional reference checks. We will keep you posted on the progress, and will let you know if we need any other information. We wish you a fruitful partnership with SkillQuo. We take great care and pride in being a true community rather than - just a platform."."\r\n"."\r\n"."We encourage you to take advantage of the many resources at your disposal, from our blog site, our weekly subscription, and stay tuned for our most exciting reveal of all: SkillQuo Cares forum. This site will serve our comprehensive support community for our SkillQuo consultants."."\r\n"."\r\n"."We will provide everything from chat rooms, to white papers to featured consultant success stories. We will update you on our offsite and onsite activities such as meet-and-greets to sweepstakes and contests. As we said, we stand by our word to you in being a true community."."\r\n"."\r\n"."With SkillQuo, freelancing is associated with exclusivity, prestige, and the brightest minds solving the most intriguing, layered business problems."."\r\n"."\r\n"."We hope you feel empowered to chart your own path, augment your income, and build your powerhouse brand."."\r\n"."\r\n"."Please feel free to reach out to us at anytime, with questions."."\r\n"."\r\n"."Sincerely,"."\r\n"."Arathi Dar, Co-Founder & CMO at SkillQuo")
			);
		}
		if(!$noProfile)
			update_post_meta($profile_id,"skillquo_approve",$value);
		else 
			update_user_meta($profile_id,"skillquo_approve",$value);
	}
	
	remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
	
	if(!$noProfile) {
		$approve = get_post_meta($profile_id,"skillquo_approve",true);
		$under_review = get_post_meta($profile_id,"under_review",true);
	} else {
		$approve = get_user_meta($profile_id,"skillquo_approve",true);
		$under_review = get_user_meta($profile_id,"under_review",true);
	}
	
	if($approve != 1 && !current_user_can( "activate_plugins" ))
		header("Location: /");

	
}

get_header();
$next_post = false;
if($convert) {
    $next_post = ae_get_adjacent_post($convert->ID, false, '', true, 'skill');
}


?>
	<section></section>
	<section class="breadcrumb-wrapper">
		<div class="breadcrumb-single-site">
        	<div class="container">
    			<div class="row">
                	<div class="col-md-6 col-xs-8">
                    	<ol class="breadcrumb">
                            <li><a href="<?php echo home_url(); ?>"><?php _e("Home", ET_DOMAIN); ?></a></li>
                            <li class="active"><?php printf(__("%s's Profile", ET_DOMAIN), $author_name); ?></li>
                        </ol>
                    </div>

                    <?php /* if($next_post) { ?>
                        <div class="col-md-6 col-xs-4">
                        	<a title="<?php the_author_meta('display_name', $next_post->post_author) ?>" href="<?php echo get_author_posts_url($next_post->post_author);  ?>" class="prj-next-link"><?php _e('Next Profile', ET_DOMAIN);?> <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    <?php }*/ ?>
                </div>
            </div>
        </div>
	</section>
	<?php
	//Check if the Profile/Project is Under Review
	if($profile_id) {
		$attachment_id = get_post_meta( $profile_id, "banner_id", true );
	} else {
		$attachment_id = get_user_meta( $user_ID, "banner_id", true );
	}
	$the_banner_url = wp_get_attachment_url( $attachment_id );

	?>
	<div class="author_the_banner" style="background-image: url(<?php echo (!empty($the_banner_url))?$the_banner_url:site_url().'/wp-content/uploads/2016/11/SkillQuo-Strategy-R1.0-ContactUs.jpg'; ?>); background-size: cover; background-position: center center;"></div>
    <div class="single-profile-wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-8">
                	<div class="tab-content-single-profile">
                    	<!-- Title -->
                    	<div class="row title-tab-profile">
                            <div class="col-md-12">
                                <h2><?php printf(__('ABOUT %s', ET_DOMAIN), strtoupper($author_name) ); ?></h2>
                            </div>
                        </div>
                        <!-- Title / End -->
                        <!-- Content project -->
                        <div class="single-profile-content">
                        	<div class="single-profile-top">
                                <ul class="single-profile">
                                    <li class="img-avatar"><span class="avatar-profile"><?php echo get_avatar($author_id, 70); ?></span></li>
                                    <li class="info-profile">
                                        <span class="name-profile"><?php echo $author_name; ?></span>
                                    <?php if($convert) { ?>
                                        <span class="position-profile"><?php echo $convert->et_professional_title; ?></span>
                                    <?php } ?>
                                        <span class="number-review-profile"><?php if($count_review < 2) printf(__('%d review', ET_DOMAIN), $count_review ); else printf(__('%d reviews', ET_DOMAIN), $count_review );?></span>
                                    </li>
                                </ul>
                                <?php // Html for Freelancer Like Button Starts (devTooba) ?>
                                <div class="list-skill-profile">
                                    <?php if( (fre_share_role() && ae_user_role($author_id) == EMPLOYER || ae_user_role($author_id) == 'administrator') || ae_user_role($author_id) == FREELANCER ){ ?>
                                        <a href="#" id="like-freelancer" class="invite-freelancer btn-sumary" data-user="<?php echo $convert->post_author ?>" style="float:right; display:none;"></a>
                                        <div style="clear:both;"></div>
                                    <?php } ?>

                                     <?php if($convert && (fre_share_role() || ae_user_role($author_id) == FREELANCER ) ){ ?>
                                            <ul>
                                                <?php if(isset($convert->tax_input['skill']) && $convert->tax_input['skill']){ ?>
                                                    <?php foreach ($convert->tax_input['skill'] as $tax){ ?>
                                                        <li><span class="skill-name-profile"><?php echo $tax->name; ?></span></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                </div>
                                <?php // end html ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="single-profile-bottom">
                                <?php if($convert) { ?>
									<!-- overview -->
									<div class="profile-overview">
										<h4 class="title-single-profile"><?php _e('Overview', ET_DOMAIN);?></h4>
										<p><?php echo $convert->post_content; ?></p>
										<?php
										if(function_exists('et_the_field')) {
											et_render_custom_meta($convert);
											et_render_custom_taxonomy($convert);
										}
										?>
									</div>
									<?php 
									
									$profile_array = array(
										"exp_stillWork",
										"exp_title",
										"exp_employer",
										"exp_location",
										"exp_fromDate",
										"exp_toDate",
										"exp_description",
										"education_school",
										"education_degree",
										"education_focus",
										"education_year"
									);
									
									foreach($profile_array as $item) {
										$$item = get_post_meta($profile_id,$item,true);
									}

									?>
									<div class="profile-overview past-experience-container" style="margin-bottom: 0; padding-bottom: 0; border: 0;">
										<h4 class="title-single-profile"><?php _e('Work Experience', ET_DOMAIN);?></h4>
										<ul class="bid-list-container" id="experience-list-container" style="padding: 0;">
										<?php 
										
											$titles = unserialize($exp_title);
											$employer = unserialize($exp_employer);
											$toDate = unserialize($exp_toDate);
											$fromDate = unserialize($exp_fromDate);
											$description = unserialize($exp_description);
											$stillWorking = unserialize($exp_stillWork);
											$experienceCount = count($titles);
										
											for($i = 0; $i < $experienceCount; $i++) {
												if($titles[$i]) {
													echo "<li id='exp_list_".$i."'>";
														if(!$under_review) { echo '<span data-featherlight="#work_'.$i.'" class="btn-edit btn-sumary">Edit</span>'; }
														echo '<span class="title">'.$titles[$i].'</span> at ';
														echo '<span class="employer">'.$employer[$i].'</span>';
														
														if(!$toDate[$i])
															echo '<span class="date">'.$fromDate[$i].'</span>';
														else
															echo '<span class="date">'.$fromDate[$i].' - '.$toDate[$i].'</span>';
													
														echo '<p>'.$description[$i].'</p>';
													echo "</li>";
												}
											}
										?>
										</ul>
									</div>
									<div class="profile-overview past-experience-container" style="margin-bottom: 0; padding-top: 0;">
										<h4 class="title-single-profile"><?php _e('Education', ET_DOMAIN);?></h4>
										<ul class="bid-list-container" id="education-list-container" style="padding: 0;">
											<?php 
											$schools = unserialize($education_school);
											$focus = unserialize($education_focus);
											$year = unserialize($education_year);
											$degree = unserialize($education_degree);
											$educationCount = count($schools);
											
											for($i = 0; $i < $educationCount; $i++) {
												if($schools[$i]) {
													echo "<li id='edu_list_".$i."' class='education'>";
														echo '<span class="title">'.$schools[$i].'</span><br/>';
														echo '<span class="employer">'.$degree[$i].'</span>';
														echo '<span class="focus">'.$focus[$i].'</span>';
														
														$attended_years = explode(",",$year[$i]);
														if($attended_years[0] && $attended_years[1]) 
															echo '<span class="date">'.$attended_years[0] .' - '. $attended_years[1].'</span>';
														else if($attended_years[0])
															echo '<span class="date">'.$attended_years[0].'</span>';
														
													echo "</li>";
												}
											}
											
											?>
										</ul>
									</div>
									<!--// overview -->
                                <?php } ?>
                                <?php if(fre_share_role() || ae_user_role($author_id) != FREELANCER ){ ?>
									<div class="profile-company project-company">

										<div class="work-company-heading">
											<h4 class="title-big-info-work-company-items">Company Information</h4>
											<div class="clearfix"></div>
										</div>
										<ul class="list-company-profile">
											<li class="bid-item out in">
												<div class="name-company" style="padding-left: 25px;">
													<div class="content-bid-item-company">									
														<h5><a href="<?php echo get_user_meta($author_id,"business_website",true); ?>"><?php echo get_user_meta($author_id,"business_name",true); ?></a></h5>
														<span class="stt-in-process">
															<?php echo get_user_meta($author_id,"business_detail",true); ?>
														</span>
														<p>
															<?php echo get_user_meta($author_id,"business_description",true); ?>
														</p>
													</div>
												</div>
												<?php $industry = get_user_meta($author_id,"industry",true); ?>
												<ul class="info-company" style="list-style: none;">
													<?php if($industry){ ?> <li><?php echo $industry;?></li> <?php } ?>
												</ul>
												<div class="clearfix"></div>
											</li>
										</ul>									
									</div>
                                <?php } ?>
								<?php 
                                if(fre_share_role() || ae_user_role($author_id) != FREELANCER ){
                                    get_template_part('template/work', 'history');
                                }
								?>
								<?php
                                if( fre_share_role() || ae_user_role($author_id) == FREELANCER ){
                                    get_template_part('template/bid', 'history');
                                    $bid_posts   = $wp_query->found_posts;
                                ?>
                                    <div class="portfolio-container">
                                    <?php
                                        query_posts( array(
                                                        // 'post_parent' => $convert->ID,
                                                        'post_status' => 'publish',
                                                        'post_type' => PORTFOLIO,
                                                        'author' => $author_id )
                                                    );
                                        if(have_posts()):
                                            get_template_part('template/portfolios', 'filter' );
                                            // list portfolio
                                            get_template_part( 'list', 'portfolios' );
                                        else :
                                        endif;
                                        //wp_reset_postdata();
                                        wp_reset_query();
                                    ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- Content project / End -->
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Title -->
                    <div class="row title-tab-profile">
                        <div class="col-md-12">
                            <h2><?php _e('INFO', ET_DOMAIN);?></h2>
                        </div>
                    </div>
                    <div class="single-profile-content">
                        <?php
                        if ( (fre_share_role() && ae_user_role($author_id) == EMPLOYER || ae_user_role($author_id) == 'administrator') || ae_user_role($author_id) == FREELANCER ){
                         if($author_available == 'on' || $author_available == '' ){ ?>
                            <div class="contact-link">
                                <a href="#" data-toggle="modal" class="invite-freelancer btn-sumary <?php if ( is_user_logged_in() ) { echo 'invite-open';}else{ echo 'login-btn';} ?>"  data-user="<?php echo $convert->post_author ?>">
                                    <?php _e("Invite me to join", ET_DOMAIN) ?>
                                </a>
                                <?php /*
                                <span><?php _e("Or", ET_DOMAIN); ?></span>
                                <a href="#" class="<?php if ( is_user_logged_in() ) {echo 'contact-me';} else{ echo 'login-btn';} ?> fre-contact"  data-user="<?php echo $convert->post_author ?>" data-user="<?php echo $convert->post_author ?>">
                                    <?php _e("Contact me", ET_DOMAIN) ?>
                                </a>
                                */
                                ?>
                            </div>
                        <?php } else {
                                echo '<h3 style="padding: 20px 25px;margin:0;">'.$author_name .'</h3>';
                            }
                            $rating = Fre_Review::freelancer_rating_score($author_id);
                        }
                        ?>
                    </div>
                    <?php if( ae_user_role($author_id) == FREELANCER ){?>
                        <!-- Title / End -->
                        <!-- Content project -->
                        <div class="single-profile-content">
                            <ul class="list-detail-info">
                            	<li>
                                    <i class="fa fa-dollar"></i>
                                    <span class="text"><?php _e('Hourly Rate:',ET_DOMAIN);?></span>
                                    <span class="text-right"><?php echo $convert->hourly_rate_price;  ?></span>
                                </li>
                                <li>
                                	<i class="fa fa-star"></i>
                                    <span class="text"><?php _e('Rating:',ET_DOMAIN);?></span>
                                	<div class="rate-it" data-score="<?php echo $rating['rating_score']; ?>"></div>
                                </li>
                                <li>
                                    <i class="fa fa-pagelines"></i>
                                    <span class="text"><?php _e('Experience:',ET_DOMAIN);?></span>
                                    <span class="text-right"><?php echo $convert->experience; ?></span>
                                </li>
                                <li>
                                    <i class="fa fa-briefcase"></i>
                                    <span class="text"><?php _e('Projects worked:',ET_DOMAIN);?></span>
                                    <span class="text-right"><?php echo $bid_posts; ?></span>
                                </li>
                                <li>
                                    <i class="fa fa-money"></i>
                                    <span class="text"><?php _e('Total earned:',ET_DOMAIN);?></span>
                                    <span class="text-right"><?php echo fre_price_format(fre_count_total_user_earned($author_id)); ?></span>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span class="text"><?php _e('Country:',ET_DOMAIN);?></span>
                                    <span class="text-right">
                                        <?php
                                        if($convert->tax_input['country']){
                                            echo $convert->tax_input['country']['0']->name;
                                        } ?>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="info-company-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php fre_display_user_info( $author_id ); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Content project / End -->
						<?php if(current_user_can( "activate_plugins" )){ ?>
                        <div class="info-company-wrapper">
                            <div class="row">
                                <div class="col-md-12">
									<ul class="list-info-company-details">
										<li>
											<div class="hired"><i class="fa fa-smile-o"></i>
												Status: 
													<?php if($approve == 1) { ?>
														<span style="color: #8BC34A;" class="info" title="">Approved</span>
													<?php } else if($approve == 2){?>
														<span style="color: #FF5252;" class="info" title="">Rejected</span>
													<?php } else if($approve == 3){?>
														<span style="color: #FF5252;" class="info" title="">Permanently Rejected</span>
													<?php } else { ?>
														<span  style="color: #2196F3;" class="info" title="">In Review</span>
													<?php } ?>
											</div>
										</li>
										<li style="text-align: right;">
											<form method="POST">
												<button value="1" name="skillquo_approve">Approve</button>
												<button value="3" name="skillquo_approve">Re-review</button>
												<button data-featherlight="#whyReject" onclick="preventDefault();">Reject</button>
											</form>
										</li>
										<?php if($under_review == 2) {?><li style="color: red;">This User has been Rejected before. Rejecting him will permanently ban him from submitting any more reviews.</li><?php } ?>
									</ul>
									</div>
                                </div>
                            </div>
                        </div>
						<div class="hide">
							<div id="whyReject">
								<form method="POST">
									<h3 style="margin: 0 0 20px 0;">Why are you rejecting?</h3>
									<input type="checkbox" name="reject_1" value="1"> Profile Image not up to standard
									<br/><input type="checkbox" name="reject_2" value="1"> Inappropriate Content
									<br/><input type="checkbox" name="reject_3" value="1"> Incomplete Profile
									<br/><input type="checkbox" name="reject_4" value="1"> Insufficient Content
									<br/><input type="checkbox" name="reject_5" value="1"> Fake or Copied Profile
									<br/><input type="checkbox" name="reject_6" value="1"> Others
									<br/><textarea rows="4" style="width: 100%; min-height: 100px;" name="reason"></textarea>
									<br/><button type="submit" name="skillquo_approve" value="2">Reject</button>
								</form>
							</div>
						</div>
						<?php } ?>

                </div>
            </div>
        </div>
    </div>

<?php

get_footer();