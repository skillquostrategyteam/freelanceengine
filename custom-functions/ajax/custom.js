jQuery(document).ready(function($) {
    var current = 0;
	var m_quo_wd = $('.m-quo-slider-custom > ul').width();
	var m_quo_lnt = $('.m-quo-slider-custom > ul').length;
	var m_quo_final = m_quo_wd*m_quo_lnt;
	$('.m-quo-slider-custom > ul').css('width', m_quo_wd);
	$('.m-quo-slider-custom').css('width', m_quo_final);
	
	$('.mtqou-contolers-custom .fa-angle-right').on('click', function(){
		mtqouright_custom();
	});
	
	$('.mtqou-contolers-custom .fa-angle-left').on('click', function(){
		mtqouleft_custom();
	});

function mtqouright_custom() {
	$('.m-quo-slider-custom > ul').eq(current++).css('margin-left', -m_quo_wd);
	if(current==m_quo_lnt) {
		current = 0;
		$('.m-quo-slider-custom > ul').css('margin-left', '');
	}
}

function mtqouleft_custom() {
	
	if(current == 0) {
		current=0;
	} else {
		$('.m-quo-slider-custom > ul').eq(current).prev().css('margin-left', '');
		current--
	}
	
}
});