<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Invoice</title>
	<meta name="generator" content="LibreOffice 4.4.7.2 (Linux)"/>
	<meta name="created" content="2014-07-16T19:50:04"/>
	<meta name="changed" content="2016-10-20T06:01:27"/>
	<meta name="AppVersion" content="15.0300"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
	<meta name="ShareDoc" content="false"/>
	<meta name="_TemplateID" content="TC062071001033"/>
	<style type="text/css">
body, div, table, thead, tbody, tfoot, tr, th, td, p {
	font-family: "Arial";
	font-size: x-small
}

</style>
	</head>

	<body>
<table cellspacing="0" border="0" style="margin:0 auto;">
      <colgroup width="13">
  </colgroup>
      <colgroup width="428">
  </colgroup>
      <colgroup span="2" width="90">
  </colgroup>
      <colgroup width="145">
  </colgroup>
      <colgroup width="13">
  </colgroup>
      <tr>
    <td colspan=2 rowspan=2 height="70" align="left" valign=bottom bgcolor="#D9D9D9"><font face="Corbel"><br>
      <img src="images/i_d8e8b2dcdaf999a6_html_68e7fe17.png" width="200" vspace="10" hspace="14" height="50"> </font></td>
    <td align="left" height="70" valign=bottom bgcolor="#D9D9D9"><font face="Corbel"><br>
      </font></td>
    <td colspan=2 height="70" align="right" valign=bottom bgcolor="#D9D9D9"><font face="Corbel" size=6 color="#7F7F7F">INVOICE</font></td>
    <td height="70" align="left" valign=bottom bgcolor="#D9D9D9"><font face="Corbel" color="#7F7F7F"><br>
      </font></td>
  </tr>
      <tr bgcolor="#D9D9D9">
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><b><font face="Corbel" size=3>PROFESSIONAL:</font></b></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Your Name]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Your Address Line 1]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><b><font face="Corbel" size=3>DATE:</font></b></td>
    <td align="left" valign=bottom sdnum="1033;1033;[$-409]MMMM D, YYYY;@"><font face="Corbel" size=3>[Enter Date]</font></td>
    <td align="left" valign=bottom sdnum="1033;1033;[$-409]MMMM D, YYYY;@"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Your Address Line 2]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><b><font face="Corbel" size=3>INVOICE #</font></b></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Invoice #]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[City, State, Zip]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="34" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=top><b><font face="Corbel" size=3>BILL TO:</font></b></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=top><b><font face="Corbel" size=3>FOR:</font></b></td>
    <td rowspan=3 align="left" valign=top><font face="Corbel" size=3>[Project Name]</font></td>
    <td align="left" valign=top><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Client's Name]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Company Name]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Street Address]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[City, State, Zip]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>[Phone]</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="23" align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-bottom: 2px solid #000000; border-top: 2px solid #000000; border-left: 2px solid #000000;" align="center" valign=middle><b><font face="Corbel" size=3>DESCRIPTION</font></b></td>
    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-top: 2px solid #000000;" align="center" valign=middle><b><font face="Corbel" size=3>HOURS</font></b></td>
    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000; border-top: 2px solid #000000;" align="center" valign=middle><b><font face="Corbel" size=3>RATE</font></b></td>
    <td style="border-bottom: 2px solid #000000; border-top: 2px solid #000000;border-right: 2px solid #000000;" align="center" valign=middle><b><font face="Corbel" size=3>AMOUNT</font></b></td>
    <td align="center" valign=middle><b><font face="Corbel" size=3><br>
      </font></b></td>
  </tr>
      <tr>
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr bgcolor="#D9D9D9">
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
  <tr>
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr bgcolor="#D9D9D9">
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr><tr>
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      <tr bgcolor="#D9D9D9">
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr><tr>
    <td bgcolor="#fff" height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000;  border-bottom: 1px solid #000000" align="right" valign=middle sdnum="1033;0;&quot;$&quot;#,##0.00"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" style="border-right:2px solid #000000;border-left:1px solid #000000;border-bottom: 1px solid #000000;" valign=middle sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3>$$</font></td>
    <td bgcolor="#fff" align="right" style=" " valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);;_(@_)"><font face="Corbel" size=3><br>
      </font></td>
  </tr>
      
  
      
      <tr>
    <td height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000; border-top: 1px solid #000000"><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-top: 1px solid #000000"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" valign=middle sdnum="1033;0;@  " style="border-right: 1px solid #000000;border-top: 1px solid #000000;"><font face="Corbel" size=3>SUBTOTAL </font></td>
    <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000; border-top: 1px solid #000000" align="right" valign=middle bgcolor="#D9D9D9" sdval="0" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* (#,##0.00);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"><font face="Corbel" size=3> $- </font></td>
    <td align="right" valign=middle sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* (#,##0.00);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000"><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" valign=middle sdnum="1033;0;@  "><font face="Corbel" size=3>OTHER </font></td>
    <td style=" border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="right" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="27" align="left" valign=middle><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-left: 2px solid #000000; border-bottom: 2px solid #000000"><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=middle style="border-bottom: 2px solid #000000"><font face="Corbel" size=3><br>
      </font></td>
    <td align="right" valign=middle sdnum="1033;0;@  " style="border-bottom: 2px solid #000000"><b><font face="Corbel" size=3>TOTAL </font></b></td>
    <td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="right" valign=middle bgcolor="#D9D9D9" sdval="0" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* (#,##0.00);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"><font face="Corbel" size=3> $- </font></td>
    <td align="right" valign=middle sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* (#,##0.00);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="26" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="26" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><br></td>
    <td colspan=3 align="left" valign=bottom><b><font face="Corbel" size=3>To settle this invoice, please follow the instructions below.</font></b></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>1. Log into your account at www.skillquo.com</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>2. Access the Project Tracker via your Dashboard</font></td>
    <td align="left" valign=bottom><u><font size=3 color="#0000FF"><br>
      </font></u></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>3. Follow the payment instruction on the Project Tracker screen</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="9" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="21" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel" size=3>Payment is due within 14 days upon receiving this invoice. For any questions please contact help@skillquo.com</font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="19" align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
    <td align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="26" align="left" valign=bottom><br></td>
    <td colspan=3 align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
    <td align="left" valign=bottom><br></td>
    <td align="left" valign=bottom><br></td>
  </tr>
      <tr>
    <td height="26" align="left" valign=bottom><font face="Corbel"><br>
      </font></td>
    <td colspan=4 align="center" valign=bottom><b><font face="Corbel" size=3 color="#152542">THANK YOU FOR YOUR BUSINESS! </font></b></td>
    <td align="left" valign=bottom><font face="Corbel" size=3><br>
      </font></td>
  </tr>
    </table>
<!-- ************************************************************************** -->
</body>
</html>
