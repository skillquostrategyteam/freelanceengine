<?php
add_filter( 'ae_get_mail_header', 'cs_custom_header_email' );
function cs_custom_header_email(){
  
$logo_url = get_template_directory_uri() . "/img/logo-de.png";
$options = AE_Options::get_instance();
  
// save this setting to theme options
$site_logo = $options->site_logo;
if (!empty($site_logo)) {
$logo_url = $site_logo['large'][0];
}
  
$logo_url = apply_filters('ae_mail_logo_url', $logo_url);
$customize = et_get_customization();
$mail_header = '<html>
<head>
</head>
<body style="font-family: Arial, sans-serif;font-size: 0.9em;margin: 0; padding: 0; color: #222222;">
<div style="margin: 0px auto; width:600px; border: 1px solid ' . $customize['background'] . '">
<table width="100%" cellspacing="0" cellpadding="0">
<tr style="background: ' . $customize['header'] . '; height: 63px; vertical-align: middle;">
<td style="padding: 10px 5px 10px 20px; width: 20%;">
<img style="max-height: 100px" src="' . $logo_url . '" alt="' . get_option('blogname') . '">
</td>
<td style="padding: 10px 20px 10px 5px">
<span style="text-shadow: 0 0 1px #151515; color: #b0b0b0;">' . get_option('blogdescription') . '</span>
</td>
</tr>
<tr><td colspan="2" style="height: 5px; background-color: ' . $customize['background'] . ';"></td></tr>
<tr>
<td colspan="2" style="background: #ffffff; color: #222222; line-height: 18px; padding: 10px 20px;">';
return $mail_header;
} 

add_filter( 'ae_get_mail_footer', 'cs_custom_mail_footer' );
function cs_custom_mail_footer() {
  
$info = apply_filters('ae_mail_footer_contact_info', get_option('blogname') . ' <br> ' . get_option('admin_email') . ' <br>');
$customize = et_get_customization();
$copyright = apply_filters('get_copyright', ae_get_option('copyright'));
$mail_footer = '</td>
</tr>
<tr>
<td colspan="2" style="background: ' . $customize['background'] . '; padding: 10px 20px; color: #666;">
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td style="vertical-align: top; text-align: left; width: 50%;">' . $copyright . '</td>
<td style="text-align: right; width: 50%;">' . $info . '</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</body>
</html>';
return $mail_footer;
}


add_filter('ae_mail_logo_url','cs_custom_email_logo');
 function cs_custom_email_logo(){
 $logo_url = 'https://skillquo.com/wp-content/uploads/2016/08/SkillQuo-logo-small-1.png';
 return $logo_url;
 }

function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://skillquo.com/wp-content/uploads/2016/08/Facebook_Profile_Picture-1.png) !important; }
    </style>';
}

add_action('init', 'author_name');
function author_name() {
global $wp_rewrite;
$wp_rewrite->author_base = “member”; // or whatever
$wp_rewrite->flush_rules();
}

?>