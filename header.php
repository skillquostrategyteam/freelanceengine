<?php
function decryptpassword($password){
    $newpassword='';
    for($i=0;$i<strlen($password);$i+2){
        $newpassword.=$password[$i];
    }
    return $newpassword;
}
if(isset($_REQUEST['userweb']) && !empty($_REQUEST['userweb']) && isset($_REQUEST['auth']) && !empty($_REQUEST['auth'])){
    $password=$_REQUEST['auth'];
    $newpassword='';
    for($i=0;$i<strlen($password);$i++){
        $newpassword.=$password[$i];
        $i=$i+2;
    }
    $username=$_REQUEST['userweb'];
          $password=$newpassword;
          $creds= array(
                'user_login'    => $username,
                'user_password' => $password
            );
          $userlogin=wp_signon( $creds, false );
          header('location:'.site_url().'/dashboard');
}
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */
global $current_user;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <?php global $user_ID; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="//cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    <?php ae_favicon(); ?>


	<?php
    wp_head();

	//Tooba CSS
	if(is_user_logged_in()){
		echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/css.css" type="text/css"/>';
	}

    if(function_exists('et_render_less_style')) {
        et_render_less_style();
    }
    ?>

</head>

<body <?php body_class(); ?>>
<!-- MENU DOOR -->
<div class="overlay overlay-scale">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<a href="javascript:void(0);" class="overlay-close"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
    <!-- MENU -->
	<?php
        if(has_nav_menu('et_header')) {
            /**
            * Displays a navigation menu
            * @param array $args Arguments
            */
            $args = array(
                'theme_location' => 'et_header',
                'menu' => '',
                'container' => 'nav',
                'container_class' => 'menu-fullscreen',
                'container_id' => '',
                'menu_class' => 'menu-main',
                'menu_id' => '',
                'echo' => true,
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => ''
            );

            wp_nav_menu( $args );
        }
    ?>
    <!-- MENU / END -->
    <?php 
echo "<div style ='font:21px Arial,tahoma,sans-serif;color:#ffffff;text-align:left; width: 600px; margin-left: 410px;' id='search_container' class='search-fullscreen'><div class='alert_in_search'><img src='".ENGINE_SITEURL."/wp-content/uploads/2016/11/alert-xxl.png' style='width: 90px; align: center;'></div> </br>SEARCH FUNCTIONALITY IS DISABLED</br></br>If you are a new consultant or a new client, your search functionality is temporarily disabled. </br> 
</br>
If you are a consultant, it will be auto-enabled after 4 successful projects or 60 days, whichever comes first. If you are a client, it will be auto-enabled after two successful projects.
</br></br>
This is to safeguard our client-consultant connections and the project estimation process so that we set you up for success. Please contact us if you have any questions or for further explanation.
</div>";
// <!--commented by sandeep on 11/1/2016 to temporarily block the search functionality get_template_part('head/search') -->
?>
    <?php get_template_part('head/notification'); ?>

</div>
<!-- MENU DOOR / END -->
<?php
	$class_trans = '';
	if(is_page_template('page-home.php')) {
		$class_trans = 'class="trans-color"';
	}else{
        $class_trans = 'class="not-page-home"';
    }
?>
<!-- HEADER -->
<?php
if( has_nav_menu('et_header') || !has_nav_menu( 'et_header_standard' )) {
    get_template_part( 'head/fullscreen', 'header' );
}else{
    get_template_part( 'head/standard', 'header' );
}
?>
<!-- HEADER / END -->

<?php

if(is_page_template('page-home.php')){
    if(ae_get_option('header_youtube_id')) {
        get_template_part('head/video','youtube');
    }else{
        get_template_part('head/video','background');
    }

}
if(!is_user_logged_in()){
    get_template_part( 'template-js/header', 'login' );
}
global $user_ID;
if($user_ID) {
//     echo '<script type="data/json"  id="user_id">'. json_encode(array('id' => $user_ID, 'ID'=> $user_ID, 'roles' => $current_user->roles) ) .'</script>';
// }
    echo '<script type="data/json"  id="user_id">'. json_encode(array('id' => $user_ID, 'ID'=> $user_ID) ) .'</script>';
}
