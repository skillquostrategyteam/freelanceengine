<?php
global $wp_query, $ae_post_factory, $post, $current_user, $user_ID;

//This Grabs the "Post ID" or the "Profile Post ID" of the User
$profile_id = get_user_meta( $user_ID, 'user_profile_id', true);

//Check if we have a POST from the History Forms such as Updating/Adding/Deleting a Experience or Education
if($_POST) {

    //This checks if we are editing an experience or education. 
    if(is_numeric($_POST["experience"]) || (isset($_POST["experience"]) && $_POST["experience"] == 0)) {

        //An array of the fields for the past history or experience
        $experience_array = array(
            "exp_stillWork",
            "exp_title",
            "exp_employer",
            "exp_fromDate",
            "exp_toDate",
            "exp_description"
        );

        //This basically converts the array above into variables. 
        //For example 'exp_stillWork' will now have '$exp_stillWork'
        //And $exp_stillWork will get its data from a Post Meta from the Profile Custom Post
        //The Data is serialize and we have to unserialize it.
        foreach($experience_array as $value) {
            $$value = unserialize(get_post_meta($profile_id,$value,true));
        }

        //We need to count how much data is in the meta data so we can properly use it for loops.
        if($exp_title[0])
            $save_count = count($exp_title);
        else
            $save_count = 0;

        //Action is our basic "CRUD" but no R because we are already reading the page.
        switch($_POST["action"]) {
            case "delete":
                unset($exp_stillWork[$_POST["experience"]]);
                unset($exp_title[$_POST["experience"]]);
                unset($exp_employer[$_POST["experience"]]);
                unset($exp_fromDate[$_POST["experience"]]);
                unset($exp_toDate[$_POST["experience"]]);
                unset($exp_description[$_POST["experience"]]);
                if(is_array($exp_title)) {
                    $exp_stillWork = array_values($exp_stillWork);
                    $exp_title = array_values($exp_title);
                    $exp_employer = array_values($exp_employer);
                    $exp_fromDate = array_values($exp_fromDate);
                    $exp_toDate = array_values($exp_toDate);
                    $exp_description = array_values($exp_description);
                }
                break;

            case "save": //Update
                $exp_stillWork[$_POST["experience"]] = sprintf("%s",$_POST["exp_stillWork"]);
                $exp_title[$_POST["experience"]] = sprintf("%s",$_POST["exp_title"]);
                $exp_employer[$_POST["experience"]] = sprintf("%s",$_POST["exp_employer"]);
                $exp_fromDate[$_POST["experience"]] = sprintf("%s",$_POST["exp_fromDate"]);
                $exp_toDate[$_POST["experience"]] = sprintf("%s",$_POST["exp_toDate"]);
                $exp_description[$_POST["experience"]] = sprintf("%s",$_POST["exp_description"]);
                break;

            case "add": //Create
                $exp_stillWork[$save_count] = sprintf("%s",$_POST["exp_stillWork"]);
                $exp_title[$save_count] = sprintf("%s",$_POST["exp_title"]);
                $exp_employer[$save_count] = sprintf("%s",$_POST["exp_employer"]);
                $exp_fromDate[$save_count] = sprintf("%s",$_POST["exp_fromDate"]);
                $exp_toDate[$save_count] = sprintf("%s",$_POST["exp_toDate"]);
                $exp_description[$save_count] = sprintf("%s",$_POST["exp_description"]);
                break;
        }

        //Update the Data! 
        //As you can see I serialize the data because we can't have multiple "exp_stillWork" columns in postmeta
        update_post_meta($profile_id, "exp_stillWork", serialize($exp_stillWork));
        update_post_meta($profile_id, "exp_title", serialize($exp_title));
        update_post_meta($profile_id, "exp_employer", serialize($exp_employer));
        update_post_meta($profile_id, "exp_toDate", serialize($exp_toDate));
        update_post_meta($profile_id, "exp_fromDate", serialize($exp_fromDate));
        update_post_meta($profile_id, "exp_description", serialize($exp_description));

        //Now if we are not updating experiences, 
        //we would be essentially almost the same thing for education
    } else if(is_numeric($_POST["education"]) || (isset($_POST["education"]) && $_POST["education"] == 0)) {

        $education_array = array(
            "education_school",
            "education_degree",
            "education_focus",
            "education_year"
        );

        foreach($education_array as $value) {
            $$value = unserialize(get_post_meta($profile_id,$value,true));
        }

        if($education_school[0])
            $save_count = count($education_school);
        else
            $save_count = 0;

        switch($_POST["action"]) {
            case "delete":
                unset($education_school[$_POST["education"]]);
                unset($education_degree[$_POST["education"]]);
                unset($education_focus[$_POST["education"]]);
                unset($education_year[$_POST["education"]]);
                $education_school = array_values($education_school);
                $education_degree = array_values($education_degree);
                $education_focus = array_values($education_focus);
                $education_year = array_values($education_year);
                break;

            case "save":
                $education_school[$_POST["education"]] = sprintf("%s",$_POST["education_school"]);
                $education_degree[$_POST["education"]] = sprintf("%s",$_POST["education_degree"]);
                $education_focus[$_POST["education"]] = sprintf("%s",$_POST["education_focus"]);
                $education_year[$_POST["education"]] = sprintf("%s",$_POST["education_year"]);
                break;

            case "add":
                $education_school[$save_count] = sprintf("%s",$_POST["education_school"]);
                $education_degree[$save_count] = sprintf("%s",$_POST["education_degree"]);
                $education_focus[$save_count] = sprintf("%s",$_POST["education_focus"]);
                $education_year[$save_count] = sprintf("%s",$_POST["education_year"]);
                break;
        }

        update_post_meta($profile_id, "education_school", serialize($education_school));
        update_post_meta($profile_id, "education_degree", serialize($education_degree));
        update_post_meta($profile_id, "education_focus", serialize($education_focus));
        update_post_meta($profile_id, "education_year", serialize($education_year));

    }

}

//Posting for PDF Parsing 
if(isset($_FILES)) {

    //Get the File's Information
    $target_file = basename($_FILES["fileToUpload"]["name"]);
    $fileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //Ensure that it's a PDF
    if(strtolower($fileType) == "pdf") {

        //Include the Library that will Parse the PDF
        include 'vendor/autoload.php';
        $parser = new \Smalot\PdfParser\Parser();
        $pdf    = $parser->parseFile($_FILES["fileToUpload"]["tmp_name"]);

        //Get the Data
        $text = $pdf->getText();
        $pages  = $pdf->getPages();

        //ParseLinkedInPDF() is located in function.php
        //It is our custom algorithm to parse the data from the PDF
        //It returns a json value
        $json = parseLinkedInPDF($pages,$text);

        //We decode the Json
        $data = json_decode($json);

        //Now we can use the data and auto update the user's data

        //This is their professional title
        $title = sprintf("%s", $data->title);
        update_post_meta($profile_id, "et_professional_title", $title);

        //This is their past history of work experiences
        foreach($data->experiences as $experience) {
            $stillWorking = sprintf("%s",$experience->stillWorking);
            $serialize["exp_stillWork"][] 	= ($stillWorking) ? true : false;
            $serialize["exp_title"][] 		= sprintf("%s",$experience->title);
            $serialize["exp_employer"][] 	= sprintf("%s",$experience->location);
            $serialize["exp_fromDate"][] 	= sprintf("%s",$experience->From);
            $serialize["exp_toDate"][] 		= sprintf("%s",$experience->To);
            $serialize["exp_description"][] = sprintf("%s",$experience->description);
        }

        //This is their past history of education
        foreach($data->education as $education) {
            $serialize["education_school"][] 	= sprintf("%s",$education->name);
            $serialize["education_degree"][] 	= sprintf("%s",$education->degree);
            $serialize["education_focus"][] 	= sprintf("%s",$education->focus);
            $serialize["education_year"][] 		= sprintf("%s",$education->attended);
        }

        //This updates the data corresponding to what was organized above.
        foreach($serialize as $key => $value) {
            update_post_meta($profile_id, $key, serialize($value));
        }

    } else {
        $error["notPDF"] = true;
    }
}

//convert current user
$ae_users  = AE_Users::get_instance();
$user_data = $ae_users->convert($current_user->data);
$user_role = ae_user_role($current_user->ID);
//convert current profile
$post_object = $ae_post_factory->get(PROFILE);
$posts = get_posts(array(
    'post_type'   => PROFILE,
    'author'      => $current_user->ID,
    'showposts'   => 1,
    'post_status' => 'publish'
));
if(!empty($posts) && isset($posts[0])){
    $profile = $post_object->convert($posts[0]);
} else {
    $profile = array('id' => 0, 'ID' => 0);
}
//get profile skills
$current_skills = get_the_terms( $profile, 'skill' );
//define variables:
$skills         = isset($profile->tax_input['skill']) ? $profile->tax_input['skill'] : array() ;
$job_title      = isset($profile->et_professional_title) ? $profile->et_professional_title : '';
$hour_rate      = isset($profile->hour_rate) ? $profile->hour_rate : '';
$currency       = isset($profile->currency) ? $profile->currency : '';
$experience     = isset($profile->et_experience) ? $profile->et_experience : '';
$hour_rate      = isset($profile->hour_rate) ? $profile->hour_rate : '';
$about          = isset($profile->post_content) ? $profile->post_content : '';
$display_name   = $user_data->display_name;
$user_available = isset($user_data->user_available) && $user_data->user_available == "on" ? 'checked' : '';
$country        = isset($profile->tax_input['country'][0]) ? $profile->tax_input['country'][0]->name : get_user_meta($current_user->ID, "country",true);
$category       = isset($profile->tax_input['project_category'][0]) ? $profile->tax_input['project_category'][0]->slug : '' ;

//Custom Skill Quo Profile Variables
$education_school 		= isset($profile->education_school) ? $profile->education_school : '';
$education_degree 		= isset($profile->education_degree) ? $profile->education_degree : '';
$education_focus 		= isset($profile->education_focus) ? $profile->education_focus : '';
$education_year 		= isset($profile->education_year) ? $profile->education_year : '';

$exp_employer 			= isset($profile->exp_employer) ? $profile->exp_employer : '';
$exp_title 				= isset($profile->exp_title) ? $profile->exp_title : '';
$exp_location 			= isset($profile->exp_location) ? $profile->exp_location : '';
$exp_description		= isset($profile->exp_description) ? $profile->exp_description : '';
$exp_fromDate 			= isset($profile->exp_fromDate) ? $profile->exp_fromDate : '';
$exp_toDate 			= isset($profile->exp_toDate) ? $profile->exp_toDate : '';
$exp_stillWork			= isset($profile->exp_stillWork) ? $profile->exp_stillWork : '';

et_get_mobile_header();
?>
    <section class="section-wrapper section-user-profile list-profile-wrapper">

        <div class="tabs-acc-details tab-profile mobile-tab-profile" id="tab_account" style="display:block">
            <div class="user-profile-avatar" id="user_avatar_container">
            <span class="image" id="user_avatar_thumbnail">
                <?php echo get_avatar( $user_data->ID, 90 ); ?>
            </span>
                <a href="#" class="icon-edit-profile-user edit-avatar-user" id="user_avatar_browse_button">
                    <i class="fa fa-pencil"></i>
                </a>
                <span class="et_ajaxnonce hidden" id="<?php echo de_create_nonce( 'user_avatar_et_uploader' ); ?>"></span>
            </div>
            <form class="form-mobile-wrapper form-user-profile" id="account_form">
                <div class="form-group-mobile">
                    <label><?php _e("Your Fullname", ET_DOMAIN) ?></label>
                    <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->
                    <input type="text" id="display_name" name="display_name" value="<?php echo $user_data->display_name ?>" placeholder="<?php _e("Full name", ET_DOMAIN); ?>">
                </div>
                <div class="form-group-mobile">
                    <label><?php _e("Location", ET_DOMAIN) ?></label>
                    <input type="text" id="location" name="location" value="<?php echo $user_data->location ?>" placeholder="<?php _e("Location", ET_DOMAIN); ?>">
                </div>
                <div class="form-group-mobile">
                    <label><?php _e("Email Address", ET_DOMAIN) ?></label>
                    <input type="text" id="user_email" value="<?php echo $user_data->user_email ?>" name="user_email" placeholder="<?php _e("Email", ET_DOMAIN); ?>">
                </div>
                <?php if(ae_get_option('use_escrow', false)) {
                    do_action( 'ae_escrow_recipient_field');
                } ?>

                <?php  fre_show_credit($user_role); ?>

                <p class="btn-warpper-bid">
                    <input type="submit" class="btn-submit btn-sumary btn-bid" value="<?php _e("Update", ET_DOMAIN) ?>" />
                </p>
            </form>
        </div>
        <!-- Tab profile details -->
        <?php if(fre_share_role() || $user_role == FREELANCER) { ?>
            <div class="tabs-profile-details tab-profile mobile-tab-profile collapse" id="tab_profile">
                <?php if(isset($_GET['loginfirst']) && $_GET['loginfirst'] == 'true'){ ?>
                    <div class="notice-first-login">
                        <p><?php _e('<i class="fa fa-warning"></i> You must complete your profile to do any activities on site', ET_DOMAIN);?></p>
                    </div>
                <?php } ?>
                <div class="pdf-group">
                    <form style="display: none;" name="pdfFormSubmitter" method="post" id="pdfFormSubmitter" enctype="multipart/form-data">
                        Upload your LinkedIn PDF:
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <input type="submit" value="Continue" name="uploadPDF">
                    </form>
                    Looking to quickly add items from your LinkedIn profile? You can import your LinkedIn resume.
                    <a target="_blank" href="https://www.skillquo.com/linkedin-profile.php"><span class="btn-submit btn-sumary btn-linkHOW">Learn how</span></a>
                    <span data-featherlight="#warningPDF" class="btn-submit btn-sumary btn-linkPDF">Upload your LinkedIn PDF</span>
                    <script>
                        jQuery("#pdfFormSubmitter").on( 'change', '#fileToUpload' , function () {
                            document.getElementById('pdfFormSubmitter').submit();
                        });
                    </script>
                </div>


                <form class="form-mobile-wrapper form-user-profile" id="profile_form">
                    <div class="form-group-mobile edit-profile-title">
                        <label><?php _e("Your Professional Title", ET_DOMAIN) ?></label>
                        <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->
                        <input type="text" id="et_professional_title" value="<?php echo $job_title; ?>" name="et_professional_title" placeholder="<?php _e("Title", ET_DOMAIN); ?>">
                    </div>
                    <div class="form-group-mobile">
                        <div class="hourly-rate-form">
                            <label><?php _e("Your Hourly Rate", ET_DOMAIN) ?></label>
                            <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->

                            <div class="group_profile_tan">
                                <input class="numberVal" type="text" id="hour_rate" name="hour_rate" value="<?php echo $hour_rate ?>" placeholder="<?php _e("e.g:30", ET_DOMAIN); ?>">
                                <?php
                                $currency = ae_get_option('content_currency');
                                if($currency){
                                    ?>
                                    <span class="currency-tan"><?php echo $currency['code']; ?></span>
                                <?php } else { ?>
                                    <span class="currency-tan"><?php _e('USD', ET_DOMAIN); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        <!--                <div class="curency-form">-->
                        <!--                    <label>--><?php //_e("Your Currency", ET_DOMAIN) ?><!--</label>-->


                        <!--                <select name="currency" disabled="true">-->
                        <!--                        --><?php
                        //                            $currency = ae_get_option('content_currency');
                        //                            if($currency){
                        //                        ?>
                        <!--                        <option value="--><?php //echo $currency['icon']; ?><!--">-->
                        <!--                            --><?php //echo $currency['code']; ?>
                        <!--                        </option>-->
                        <!--                        --><?php //} else { ?>
                        <!--                        <option value="--><?php //_e('$', ET_DOMAIN);?><!--">-->
                        <!--                            --><?php //_e('USD', ET_DOMAIN); ?>
                        <!--                        </option>-->
                        <!--                        --><?php //} ?>
                        <!--                    </select>-->
                        <!---->
                        <!--                </div>-->



                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group-mobile skill-profile-control">

                        <?php
                        $switch_skill = ae_get_option('switch_skill');
                        if(!$switch_skill){
                            ?>
                            <div class="wrapper-skill">
                                <label><?php _e("Your Skills", ET_DOMAIN) ?></label>
                                <a href="#" class="btn-sumary btn-add-skill add-skill"><?php _e("Add", ET_DOMAIN) ?></a>
                                <input type="text" id="skill" class="skill" placeholder="<?php _e("Skills", ET_DOMAIN); ?>">
                            </div>
                            <div class="clearfix"></div>
                            <ul class="list-skill skills-list" id="skills_list"></ul>
                            <?php
                        }else{
                            ?>
                            <div class="wrapper-skill">
                                <label><?php _e("Your Skills", ET_DOMAIN) ?></label>
                            </div>
                            <?php
                            $c_skills = array();
                            if(!empty($current_skills)){
                                foreach ($current_skills as $key => $value) {
                                    $c_skills[] = $value->term_id;
                                };
                            }
                            ae_tax_dropdown( 'skill' , array(  'attr' => 'data-chosen-width="95%" data-chosen-disable-search="" multiple data-placeholder="'.sprintf(__("Skills (max is %s)", ET_DOMAIN), ae_get_option('fre_max_skill', 5)).'"',
                                    'class'             => 'experience-form chosen multi-tax-item tax-item required',
                                    'hide_empty'        => false,
                                    'hierarchical'      => false ,
                                    'id'                => 'skill' ,
                                    'show_option_all'   => false,
                                    'selected'          => $c_skills
                                )
                            );
                        }
                        ?>
                    </div>
                    <div class="form-group-mobile">
                        <label><?php _e("Category", ET_DOMAIN) ?></label>
                        <?php
                        $cate_arr = array();
                        if(!empty($profile->tax_input['project_category'])){
                            foreach ($profile->tax_input['project_category'] as $key => $value) {
                                $cate_arr[] = $value->term_id;
                            };
                        }
                        ae_tax_dropdown( 'project_category' ,
                            array(
                                'attr'            => 'data-chosen-width="95%" multiple data-chosen-disable-search="" data-placeholder="'.__("Choose categories", ET_DOMAIN).'"',
                                'class'           => 'experience-form chosen multi-tax-item tax-item required',
                                'hide_empty'      => false,
                                'hierarchical'    => true ,
                                'id'              => 'project_category' ,
                                'selected'        => $cate_arr,
                                'show_option_all' => false
                            )
                        );
                        ?>
                    </div>
                    <?php if(fre_share_role() || $user_role == FREELANCER){ ?>
                        <div class="form-group-mobile">
                            <label class="et-receive-mail" for="et_receive_mail"><input type="checkbox" id="et_receive_mail" name="et_receive_mail_check" <?php echo (isset($profile->et_receive_mail) &&$profile->et_receive_mail == '1') ? 'checked': '' ;?>/><?php _e("Receive emails about projects that match your categories", ET_DOMAIN) ?></label>
                            <input type="hidden" value="<?php echo (isset($profile->et_receive_mail)) ? $profile->et_receive_mail : '';?>" id="et_receive_mail_value" name="et_receive_mail" />
                        </div>
                    <?php } ?>
                    <div class="form-group-mobile">
                        <label><?php _e("Country", ET_DOMAIN) ?></label>
                        <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->
                        <?php if(!ae_get_option('switch_country')){ ?>
                            <input class="" type="text" id="country" placeholder="<?php _e("Country", ET_DOMAIN); ?>" name="country" value="<?php if($country){echo $country;} ?>" autocomplete="off" class="country" spellcheck="false" >
                        <?php }else{
                            $country_arr = array();
                            if(!empty($profile->tax_input['country'])){
                                foreach ($profile->tax_input['country'] as $key => $value) {
                                    $country_arr[] = $value->term_id;
                                };
                            }
                            ae_tax_dropdown( 'country' ,
                                array(
                                    'attr'            => 'data-chosen-width="100%" multiple data-chosen-disable-search="" data-placeholder="'.__("Choose country", ET_DOMAIN).'"',
                                    'class'           => 'experience-form chosen multi-tax-item tax-item required country_profile',
                                    'hide_empty'      => false,
                                    'hierarchical'    => true ,
                                    'value'           => 'slug',
                                    'id'              => 'country' ,
                                    'selected'        => $country,
                                    'show_option_all' => false
                                )
                            );
                        }
                        ?>
                    </div>
                    <div class="form-group-mobile about-form">
                        <label><?php _e("About You", ET_DOMAIN) ?></label>
                        <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->
                        <textarea name="post_content" id="post_content" placeholder="<?php _e("About", ET_DOMAIN); ?>" rows="7"><?php echo trim(strip_tags($about)) ?></textarea>
                    </div>
                    <div class="form-group-mobile">
                        <label><?php _e("Your Experience", ET_DOMAIN) ?></label>
                        <!-- <a href="#" class="icon-edit-profile-user edit-info-user"><i class="fa fa-pencil"></i></a> -->
                        <input type="text" name="et_experience" value="<?php echo $experience; ?>" />
                    </div>

                    <?php if(fre_share_role() || $user_role == FREELANCER){ ?>
                        <div class="info-project-items past-experience-container">
                            <h4 class="title-big-info-project-items" style="padding-left: 0;">Past History</h4>
                            <ul class="bid-list-container">

                                <?php

                                $titles = unserialize($exp_title);
                                $employer = unserialize($exp_employer);
                                $toDate = unserialize($exp_toDate);
                                $fromDate = unserialize($exp_fromDate);
                                $description = unserialize($exp_description);
                                $stillWorking = unserialize($exp_stillWork);
                                $experienceCount = count($titles);

                                for($i = 0; $i < $experienceCount; $i++) {
                                    if($titles[$i]) {
                                        echo "<li>";
                                        echo '<span data-featherlight="#work_'.$i.'" class="btn-submit btn-sumary">Edit</span>';
                                        echo '<span class="title">'.$titles[$i].'</span> at ';
                                        echo '<span class="employer">'.$employer[$i].'</span>';

                                        if(!$toDate[$i])
                                            echo '<span class="date">'.$fromDate[$i].'</span>';
                                        else
                                            echo '<span class="date">'.$fromDate[$i].' - '.$toDate[$i].'</span>';

                                        echo '<p>'.$description[$i].'</p>';
                                        echo "</li>";
                                    }
                                }

                                ?>

                            </ul>
                            <span data-featherlight="#emptyExp" class="btn-submit btn-sumary btn-add">Add</span>
                        </div>
                        <div class="info-project-items past-experience-container">
                            <h4 class="title-big-info-project-items" style="padding-left: 0;">Education</h4>
                            <ul class="bid-list-container">
                                <?php

                                $schools = unserialize($education_school);
                                $focus = unserialize($education_focus);
                                $year = unserialize($education_year);
                                $degree = unserialize($education_degree);
                                $educationCount = count($schools);

                                for($i = 0; $i < $educationCount; $i++) {
                                    if($schools[$i]) {
                                        echo "<li class='education'>";
                                        echo '<span data-featherlight="#school_'.$i.'" class="btn-submit btn-sumary">Edit</span>';
                                        echo '<span class="title">'.$schools[$i].'</span> at ';
                                        echo '<span class="employer">'.$degree[$i].'</span>';
                                        echo '<span class="focus">'.$focus[$i].'</span>';
                                        echo '<span class="date">'.$year[$i].'</span>';
                                        echo "</li>";
                                    }
                                }

                                ?>
                            </ul>
                            <span data-featherlight="#emptyEducation" class="btn-submit btn-sumary btn-add">Add</span>
                        </div>
                        <div class="clearfix"></div>
                    <?php } ?>


                    <div class="form-group-mobile">
                        <?php do_action( 'ae_edit_post_form', PROFILE, $profile ); ?>
                    </div>
                    <p class="btn-warpper-bid tantan">
                        <input type="submit" class="btn-submit btn-sumary btn-bid" value="<?php _e("Update", ET_DOMAIN) ?>" />
                    </p>
                </form>
                <div class="form-group-mobile tantan">
                    <label><?php _e("Your Portfolio", ET_DOMAIN) ?></label>
                    <div class="edit-portfolio-container">
                        <?php
                        // list portfolio
                        query_posts( array(
                            'post_status' => 'publish',
                            'post_type'   => 'portfolio',
                            'author'      => $current_user->ID
                        ));
                        get_template_part( 'mobile/list', 'portfolios' );
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="tabs-project-details tab-profile mobile-tab-profile collapse" id="tab_project">
            <form class="form-mobile-wrapper form-user-profile">
                <div class="form-group-mobile edit-profile-title user-profile-history info-project-items">
                    <?php if( $user_role == FREELANCER || fre_share_role() ){ ?>
                    <!-- BIDDING -->
                    <label>
                        <?php _e("Current bids", ET_DOMAIN) ?>
                    </label>
                    <div class="list-user-bids " id="list-user-bid-wrapper">
                        <?php
                        query_posts( array(
                            'post_status' => array('publish','accept'),
                            'post_type'   => 'bid',
                            'author'      => $current_user->ID,
                        ));
                        if(have_posts()){
                            get_template_part( 'mobile/list', 'user-bids' );
                        } else {
                            echo '<span class="no-results">';
                            _e( "No current bids.", ET_DOMAIN );
                            echo '</span>';
                        }
                        wp_reset_query();
                        ?>
                    </div>
                    <label>
                        <?php _e('Your Worked History and Reviews', ET_DOMAIN) ?>
                    </label>
                    <div class="list-bid-history" id="list-bid-history-wrapper">
                        <?php
                        query_posts( array(  'post_status' => array('accept', 'complete'),
                                'post_type' => BID,
                                'author' => $current_user->ID,
                                'accepted' => 1
                            )
                        );
                        get_template_part('mobile/template/bid', 'history-list');
                        wp_reset_query();

                        } else {
                            get_template_part('mobile/template/work', 'history');
                        }
                        ?>
                    </div>
                    <!-- / END BIDDING -->
                </div>
            </form>
        </div>
        <!--TAB CREDITS-->
        <!-- Messages -->
        <?php do_action('fre_profile_mobile_tab_content');?>
        <!-- Messages / END -->

        <!-- Notification -->
        <section class="notification-section tab-profile mobile-tab-profile" id="tab_notification">
            <div class="container">
                <div class="notification-wrapper" id="notification_container">
                    <?php fre_user_notification($user_ID); ?>

                </div>
            </div>
        </section>
        <!-- Notification / END -->

        <div class="tabs-acc-details tab-profile collapse" id="tab_change_pw">
            <form class="form-mobile-wrapper form-user-profile chane_pass_form" id="chane_pass_form">
                <div class="form-group-mobile edit-profile-title">
                    <label><?php _e("Your Old Password", ET_DOMAIN) ?></label>
                    <input type="password" id="old_password" name="old_password" placeholder="<?php _e("Old password", ET_DOMAIN); ?>">
                </div>
                <div class="form-group-mobile">
                    <label><?php _e("Your New Password", ET_DOMAIN) ?></label>
                    <input type="password" id="new_password" name="new_password" placeholder="<?php _e("New password", ET_DOMAIN); ?>">
                </div>
                <div class="form-group-mobile">
                    <label><?php _e("Retype New Password", ET_DOMAIN) ?></label>
                    <input type="password" id="renew_password" name="renew_password" placeholder="<?php _e("Retype again", ET_DOMAIN); ?>">
                </div>
                <p class="btn-warpper-bid">
                    <input type="submit" class="btn-submit btn-sumary btn-bid" value="<?php _e("Change", ET_DOMAIN) ?>" />
                </p>
            </form>
        </div>
    </section>

    <!-- CURRENT PROFILE -->
<?php if(!empty($posts) && isset($posts[0])){ ?>
    <script type="data/json" id="current_profile">
    <?php echo json_encode($profile) ?>
</script>
<?php } ?>
    <!-- END / CURRENT PROFILE -->

    <!-- CURRENT SKILLS -->
<?php if( !empty($current_skills) ){ ?>
    <script type="data/json" id="current_skills">
    <?php echo json_encode($current_skills) ?>
</script>
<?php } ?>
    <!-- END / CURRENT SKILLS -->

    <div class="hide">
        <?php

        for($i = 0; $i < $experienceCount; $i++) {
            if($titles[$i]) {
                echo '<div id="work_'.$i.'">';
                ?>

                <form class="form-horizontal" method="post">
                    <fieldset>
                        <input type="hidden" value="<?php echo $i; ?>" name="experience" />
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_employer">Employer</label>
                            <div class="col-md-8">
                                <input name="exp_employer" type="text" placeholder="Employer" value="<?php echo $employer[$i] ?>" class="form-control input-md" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_title">Title</label>
                            <div class="col-md-8">
                                <input name="exp_title" type="text" placeholder="Title" value="<?php echo $titles[$i] ?>" class="form-control input-md" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_toDate">Start Date</label>
                            <div class="col-md-8">
                                <input name="exp_toDate" type="text" placeholder="Start Date" value="<?php echo $fromDate[$i] ?>" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_fromDate">Last Date</label>
                            <div class="col-md-8">
                                <input name="exp_fromDate" type="text" placeholder="Last Date" value="<?php echo $toDate[$i] ?>" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_description">Job Summary</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="5" name="exp_description"><?php echo $description[$i] ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="exp_stillWork"></label>
                            <div class="col-md-8">
                                <label class="checkbox-inline" for="exp_stillWork-0">
                                    <input type="checkbox" name="exp_stillWork" <?php if($stillWorking[$i]) echo "checked";  ?> value="1">
                                    Still work here?
                                </label>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <button onclick="jQuery.('#work_<?php echo $i; ?>').submit()" name="action" class="btn btn-danger" value="delete" name="action">Delete</button>
                                <button onclick="jQuery.('#work_<?php echo $i; ?>').submit()" value="save" name="action" class="btn btn-success">Save</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

                <?
                echo "</div>";
            }
        }

        for($i = 0; $i < $educationCount; $i++) {
            if($schools[$i]) {

                echo '<div id="school_'.$i.'">';
                ?>
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <input type="hidden" value="<?php echo $i; ?>" name="education" />
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="education_school">School Name</label>
                            <div class="col-md-8">
                                <input value="<?php echo $schools[$i] ?>" name="education_school" type="text" placeholder="What school did you go to?" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="education_focus">Major</label>
                            <div class="col-md-8">
                                <input value="<?php echo $focus[$i] ?>"  name="education_focus" type="text" placeholder="What did you focus on?" class="form-control input-md">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="education_degree">Degree</label>
                            <div class="col-md-8">
                                <input value="<?php echo $degree[$i] ?>"  name="education_degree" type="text" placeholder="What degree were you working on?" class="form-control input-md">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="education_year">Attended Date</label>
                            <div class="col-md-8">
                                <input value="<?php echo $year[$i] ?>"  name="education_year" type="text" placeholder="How long did you attend for?" class="form-control input-md">

                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <button onclick="jQuery.('#work_<?php echo $i; ?>').submit()" name="action" class="btn btn-danger" value="delete" name="action">Delete</button>
                                <button onclick="jQuery.('#work_<?php echo $i; ?>').submit()" value="save" name="action" class="btn btn-success">Save</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

                <?php
                echo '</div>';
            }
        }

        ?>

        <div id="emptyExp">
            <form class="form-horizontal" method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_employer">Employer</label>
                        <div class="col-md-8">
                            <input name="exp_employer" type="text" placeholder="Employer" class="form-control input-md" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_title">Title</label>
                        <div class="col-md-8">
                            <input name="exp_title" type="text" placeholder="Title" class="form-control input-md" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_toDate">Start Date</label>
                        <div class="col-md-8">
                            <input name="exp_toDate" type="text" placeholder="Start Date" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_fromDate">Last Date</label>
                        <div class="col-md-8">
                            <input name="exp_fromDate" type="text" placeholder="Last Date" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_description">Job Summary</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="exp_description" rows="5" placeholder="What did you do here?"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="exp_stillWork"></label>
                        <div class="col-md-8">
                            <label class="checkbox-inline" for="exp_stillWork-0">
                                <input type="checkbox" name="exp_stillWork" value="1">
                                Still work here?
                            </label>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <div class="col-md-12">
                            <button onclick="jQuery.('#emptyExp form').submit()" name="action" value="add" class="btn btn-primary">Add</button>
                        </div>
                    </div>

                    <input type="hidden" value="1" name="experience" />

                </fieldset>
            </form>
        </div>
        <div id="emptyEducation">
            <form class="form-horizontal" method="post">
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="education_school">School Name</label>
                        <div class="col-md-8">
                            <input name="education_school" type="text" placeholder="What school did you go to?" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="education_focus">Major</label>
                        <div class="col-md-8">
                            <input name="education_focus" type="text" placeholder="What did you focus on?" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="education_degree">Degree</label>
                        <div class="col-md-8">
                            <input name="education_degree" type="text" placeholder="What degree were you working on?" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="education_year">Attended Date</label>
                        <div class="col-md-8">
                            <input id="education_year" name="education_year" type="text" placeholder="How long did you attend for?" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group text-right">
                        <div class="col-md-12">
                            <button onclick="jQuery.('#emptyEducation form').submit()" name="action" value="add" class="btn btn-primary">Add</button>
                        </div>
                    </div>

                    <input type="hidden" value="1" name="education" />

                </fieldset>
            </form>

        </div>
        <div id="warningPDF">
            <p>
                Note that any information you choose to sync from your PDF will
                overwrite anything you already have in the corresponding section
                of your Skillquo profile.
            </p>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <button onclick="jQuery.featherlight.close();" name="singlebutton" class="btn btn-danger">Cancel</button>
                    <button onclick="jQuery('#fileToUpload').click();" name="singlebutton" class="btn btn-success">Continue</button>
                </div>
            </div>
        </div>
    </div>

<?php
et_get_mobile_footer();
?>