<?php
// Pull the config from environment if not set use default listed
define('SALES_SITEURL',getenv('SALES_SITEURL') ? getenv('SALES_SITEURL')  : 'http://skillquotest.com');
/**
 * Template Name: API Check if Logged On
 */
    global $current_user;
	
header('Access-Control-Allow-Origin: '.SALES_SITEURL);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
	
	if($current_user->ID) {
		$response["logged_on"] = true;
	} else {
		$response["logged_on"] = false;
	}
	
	wp_send_json( $response );
?>
