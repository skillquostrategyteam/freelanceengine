<?php
/**
 * Template Name: Member Profile Page
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */
    global $wp_query, $ae_post_factory, $post, $current_user, $user_ID;
	
	//This Grabs the "Post ID" or the "Profile Post ID" of the User
    $profile_id = get_user_meta( $user_ID, 'user_profile_id', true);
	
	$user_role = $current_user->roles[0];
	
	//If we don't have a profile id, we need to make one if the user is a Freelancer
	if(!$profile_id && $user_role == "freelancer") {
		
		if($current_user->user_firstname)
			$name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
		
		if(empty($name)) {
			$name = $current_user->display_name;
		}
	
		$empty_profile = array(
			'post_title' => $name,
			'post_type'=> "fre_profile",
			'post_status' => 'publish'
		);
		$profile_id = wp_insert_post( $empty_profile );
		update_user_meta( $user_ID, "user_profile_id", $profile_id);
	}
	
	//Check if the Profile/Project is Under Review
	if($profile_id) {
		$status = get_post_meta($profile_id, "skillquo_approve", true);
	} else {
		$status = get_user_meta($user_ID, "skillquo_approve", true);
	}
	
	//Perma Rejection
	if($status == 3) {
		$permaban = true;
		$status = 0;
		$under_review = 1;
	}
	
	//Submit for Review
	if(isset($_POST['submitForReview']) && $_POST["review"] >= 1) {
		if(!$permaban) {
			if($profile_id) {
				update_post_meta( $profile_id, "under_review", 1 );
				if($status == 2) {
					$status = 0;
					update_post_meta($profile_id, "skillquo_approve", 0);
					update_post_meta($profile_id, "under_review", 2 );
				}
			} else {
				update_user_meta( $user_ID, "under_review", 1 ); //1 = Under Review, 0 = Approve, 2 = Rejected
				if($status == 2) {
					$status = 0;
					update_user_meta($user_ID, "skillquo_approve", 0);
					update_user_meta($user_ID, "under_review", 2 );
				}
			}
		}
	}
	
	//Check if the Profile/Project is Under Review
	if($profile_id) {
		$attachment_id = get_post_meta( $profile_id, "banner_id", true );
		$under_review = get_post_meta( $profile_id, "under_review", true );
	} else {
		$attachment_id = get_user_meta( $user_ID, "banner_id", true );
		$under_review = get_user_meta( $user_ID, "under_review", true ); 
	}
	
	//Over ride the under review status if Status was rejected or Approve.
	if($status != 0) {
		$under_review = 0;
	}
	
	if($under_review == 0) {
		unset($under_review);
	}
	

	// Check that the nonce is valid, and the user can edit this post.
	if ( 
		isset( $_POST['my_image_upload_nonce'] ) 
		&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'user_banner' )
	) {
		// The nonce was valid and the user has the capabilities, it is safe to continue.

		// These files need to be included as dependencies when on the front end.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
		// Let WordPress handle the upload.
		if($profile_id)
			$attachment_id = media_handle_upload( 'user_banner', $profile_id  );
		else
			$attachment_id = media_handle_upload( 'user_banner', 0 );
		
		if ( !is_wp_error( $attachment_id ) ) {
			if($profile_id) {
				update_post_meta( $profile_id, "banner_id", $attachment_id );
			} else {
				update_user_meta( $user_ID, "banner_id", $attachment_id ); //1 = Under Review, 0 = Approve, 2 = Rejected
			}
			
		}
	}
	
	$the_banner = wp_get_attachment_image( $attachment_id );
	$the_banner_url = wp_get_attachment_url( $attachment_id );
	
	//Posting for PDF Parsing 
	if(isset($_FILES) && $profile_id) {
		
		//Get the File's Information
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
				
		//Ensure that it's a PDF
		if(strtolower($fileType) == "pdf") {

			//This library only seems to work in Linux Enviroment
			//We will be using it to decode Skills in the PDF as PDFParser likes to put Skills and Education in one line and make the parsing buggy
			require('class.pdf2text.php');

			$pdfToText = new PDF2Text();
			$pdfToText->setFilename($_FILES["fileToUpload"]["tmp_name"]); 
			$pdfToText->decodePDF();
								
			//Divide each line into a variable
			$skillText = preg_split("/((\r?\n)|(\r\n?))/", $pdfToText->output());
			foreach($skillText as $text) {
				
				//If we pass the Skill line, these lines are skills and expertise lines so long as the education flag hasn't been passed.
				if($skillFlag == true && $educationFlag == false) {
					
					if($text == " ") {
						unset($pdfParserTwo["skills"][count($pdfParserTwo["skills"]) - 1]);
						break;
					}
					
					if($text == "Education") {
						break;
					}
					
					$pdfParserTwo["skills"][] = $text;
				}
				
				if (strpos($text, "Skills & Expertise") !== false) {
					$skillFlag = true;
				}				
			}
				
			function length_sort($a,$b){
				return strlen($b)-strlen($a);
			}
			
			if(is_array($pdfParserTwo["skills"]))
			usort($pdfParserTwo["skills"],'length_sort');
			
			//
			// Parser #2
			//
			//Include the Second Library that will Parse the PDF
			include 'vendor/autoload.php';
			$parser = new \Smalot\PdfParser\Parser();
			$pdf    = $parser->parseFile($_FILES["fileToUpload"]["tmp_name"]);
			
			//Get the Data
			$text = $pdf->getText();
			$pages  = $pdf->getPages();
		
			//ParseLinkedInPDF() is located in function.php
			//It is our custom algorithm to parse the data from the PDF
			//It returns a json value
			$json = parseLinkedInPDF($pages,$text);
			
			//We decode the Json
			$data = json_decode($json);
			
			//Now we can use the data and auto update the user's data
			
			//This is their professional title
			$title = sprintf("%s", $data->title);
			update_post_meta($profile_id, "et_professional_title", $title);
			
			//Update the "Full name" and "About me"
			$full_name = sprintf("%s",$data->name);
			$summary = sprintf("%s",$data->summary);
			//wp_update_user( array( 'ID' => $profile_id, 'display_name' => $full_name ) );
			$my_post = array(
			  'ID'           => $profile_id,
			  //'post_title'   => $full_name, //This is base on Display Name and Display Name can not be updated properly
			  'post_content' => $summary
			);
			wp_update_post( $my_post );
			
			
			wp_delete_object_term_relationships( $profile_id, "skill" );
			wp_set_object_terms( $profile_id, $pdfParserTwo["skills"] , "skill" );

			//This is their past history of work experiences
			if($data->experiences)
			foreach($data->experiences as $experience) {
				$stillWorking = sprintf("%s",$experience->stillWorking);
				$serialize["exp_stillWork"][] 	= ($stillWorking) ? true : false;
				$serialize["exp_title"][] 		= sprintf("%s",$experience->title);
				$serialize["exp_employer"][] 	= sprintf("%s",$experience->location);
				$serialize["exp_fromDate"][] 	= sprintf("%s",$experience->From);
				$serialize["exp_toDate"][] 		= sprintf("%s",$experience->To);
				$serialize["exp_description"][] = sprintf("%s",$experience->description);
			}
			
			//This is their past history of education
			if($data->education)
			foreach($data->education as $education) {
				if(strpos($education->name,"Skills & Expertise") !== false) {
					$educationName = str_replace("Skills & Expertise","",$education->name);
					$educationName = str_replace($pdfParserTwo["skills"],"",$educationName);
				} else {
					$educationName = $education->name;
				}
				$serialize["education_school"][] 	= sprintf("%s",$educationName);
				$serialize["education_degree"][] 	= sprintf("%s",$education->degree);
				$serialize["education_focus"][] 	= sprintf("%s",$education->focus);
				
				
				$yearToday = date("Y",time()) + 7;
				
				for($i = 1900; $i <= $yearToday; $i++) {
					
					if(strpos($education->attended,strval($i)) !== false) {
						$edu_year[] = $i;
					}
				}
				
				if(!$edu_year[1]) 
					$edu_year[] = "-";
				
				$edu_years = implode(",",$edu_year);
				
				$serialize["education_year"][] 		= sprintf("%s",$edu_years);
				unset($edu_years);
				unset($edu_year);
			}
			
			//This updates the data corresponding to what was organized above.
			if($serialize)
			foreach($serialize as $key => $value) {
				update_post_meta($profile_id, $key, serialize($value));
			}
					
		} else {
			$error = "Not a PDF";
		}
	}
		
    //convert current user
    $ae_users  = AE_Users::get_instance();	
    $user_data = $ae_users->convert($current_user->data);
    $user_role = ae_user_role($current_user->ID);

    //convert current profile
    $post_object = $ae_post_factory->get(PROFILE);
    $profile = array('id' => 0, 'ID' => 0);
    if($profile_id) {
        $profile_post = get_post( $profile_id );
        if($profile_post && !is_wp_error( $profile_post )){
            $profile = $post_object->convert($profile_post);
        }
    }
		
	if($_POST["skills"] && $profile_id) {
		wp_delete_object_term_relationships( $profile_id, "skill" );
		wp_set_object_terms( $profile_id, $_POST["skills"] , "skill" );
	}
		
    //get profile skills
    $current_skills = get_the_terms( $profile, 'skill' );
    //define variables:
    $skills         = isset($profile->tax_input['skill']) ? $profile->tax_input['skill'] : array() ;	
    $job_title      = isset($profile->et_professional_title) ? $profile->et_professional_title : '';
    $hour_rate      = isset($profile->hour_rate) ? $profile->hour_rate : '';
    $currency       = isset($profile->currency) ? $profile->currency : '';
    $experience     = isset($profile->et_experience) ? $profile->et_experience : '';
    $about          = isset($profile->post_content) ? $profile->post_content : '';
    $display_name   = $user_data->display_name;
    $user_available = isset($user_data->user_available) && $user_data->user_available == "on" ? 'checked' : '';
    $country        = isset($profile->tax_input['country'][0]) ? $profile->tax_input['country'][0]->name : get_user_meta($current_user->ID, "country",true);
    $category       = isset($profile->tax_input['project_category'][0]) ? $profile->tax_input['project_category'][0]->slug : '' ;

	if(!isset($country) || empty($country)) {
		$country = "america";
	}

	if($user_role == FREELANCER) {
   //Custom Skill Quo Profile Variables
   /*
	$education_school 		= isset($profile->education_school) ? $profile->education_school : '';
	$education_degree 		= isset($profile->education_degree) ? $profile->education_degree : '';
	$education_focus 		= isset($profile->education_focus) ? $profile->education_focus : '';
	$education_year 		= isset($profile->education_year) ? $profile->education_year : '';
	
	$exp_employer 			= isset($profile->exp_employer) ? $profile->exp_employer : '';
	$exp_title 				= isset($profile->exp_title) ? $profile->exp_title : '';
	$exp_location 			= isset($profile->exp_location) ? $profile->exp_location : '';
	$exp_description		= isset($profile->exp_description) ? $profile->exp_description : '';
	$exp_fromDate 			= isset($profile->exp_fromDate) ? $profile->exp_fromDate : '';
	$exp_toDate 			= isset($profile->exp_toDate) ? $profile->exp_toDate : '';
	$exp_stillWork			= isset($profile->exp_stillWork) ? $profile->exp_stillWork : '';
	*/
	
	$profile_array = array(
		"exp_stillWork",
		"exp_title",
		"exp_employer",
		"exp_location",
		"exp_fromDate",
		"exp_toDate",
		"exp_description",
		"education_school",
		"education_degree",
		"education_focus",
		"education_year"
	);
	
	foreach($profile_array as $item) {
		$$item = get_post_meta($profile_id,$item,true);
	}

	unset($item);
	
	} else {
		
		$quoCustomFields = array(
			"country",
			"business_name",
			"business_detail",
			"business_description",
			"business_website",
			"business_type",
			"industry",
			"hearAboutUs",
			"employee_amount",
			"city",
			"title",
			"postal_code"
		);
		
		foreach($quoCustomFields as $value) {
			 $user_data->$value = get_user_meta($current_user->ID, $value,true);
		}
		
	}
	
	get_header();

    // Handle email change requests
    $user_meta = get_user_meta($user_ID, 'adminhash', true);

    if(! empty($_GET[ 'adminhash' ] )){
        if(is_array($user_meta) && $user_meta['hash'] == $_GET['adminhash'] && !empty($user_meta[ 'newemail' ])){
            wp_update_user(array('ID' => $user_ID,
                'user_email' => $user_meta['newemail']
            ));
            delete_user_meta( $user_ID, 'adminhash' );
        }
        echo "<script> window.location.href = '".et_get_page_link("profile")."'</script>";
    }elseif(! empty($_GET[ 'dismiss' ] ) && 'new_email' == $_GET['dismiss']){
        delete_user_meta( $user_ID, 'adminhash' );
        echo "<script> window.location.href = '".et_get_page_link("profile")."'</script>";
    }

?>

<?php ?>

<section class="section-wrapper <?php if($user_role == FREELANCER) echo 'freelancer'; ?>">
	
	<div class="number-profile-wrapper" style="background-image: url(<?php echo (!empty($the_banner_url))?$the_banner_url:site_url().'/wp-content/uploads/2016/11/SkillQuo-Strategy-R1.0-ContactUs.jpg'; ?>); background-size: cover; background-position: center center;">
	
	
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<h2 class="number-profile"><?php printf(__(" <span>%s's</span> Profile ", ET_DOMAIN), $display_name ) ?></h2>
                    <div class="nav-tabs-profile">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs responsive" role="tablist" id="myTab">
                            <li class="active">
                                <a href="#tab_account_details" role="tab" data-toggle="tab">
                                    <?php _e('Account Details', ET_DOMAIN) ?>
                                </a>
                            </li>
                            <?php if(fre_share_role() || $user_role == FREELANCER){ ?>
                            <li>
                                <a href="#tab_profile_details" role="tab" data-toggle="tab">
                                    <?php _e('Profile Details', ET_DOMAIN) ?>
                                </a>
                            </li>
                            <?php } ?>
                            <li>
                                <a href="#tab_project_details" role="tab" data-toggle="tab">
                                    <?php _e('Project Details', ET_DOMAIN) ?>
                                </a>
                            </li>
                            <li>
                               <a href="#tab_inbox" role="tab" data-toggle="tab">
                                    <?php _e('Inbox', ET_DOMAIN) ?>
                               </a>
                           	</li>
							<?php if(!$under_review && !$permaban) { ?>
							<li data-featherlight="#warningBanner" class="upload_banner_btn">
								Upload a Banner
							</li>
							<form style="display: none;" id="featured_upload" method="post" action="#" enctype="multipart/form-data">
								<input type="file" name="user_banner" id="user_banner"  multiple="false" />
								<?php wp_nonce_field( 'user_banner', 'my_image_upload_nonce' ); ?>
								<input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload" />
							</form>
							<script>
								jQuery("#featured_upload").on( 'change', '#user_banner' , function () {
									jQuery(".featherlight-content #warningBanner").slideUp(500,function(){
										jQuery(".featherlight-content #warningBanner").html("Your banner is being uploaded <img src='/wp-content/themes/freelanceengine/includes/aecore/assets/img//loading.gif' />").slideDown(1000);
									});
									document.getElementById('featured_upload').submit();
								});
							</script>
							<?php } ?>
                            <?php do_action('fre_profile_tabs'); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="list-profile-wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-8">
                	<div class="tab-content-profile">
                        <!-- Tab panes -->
                        <div class="tab-content block-profiles responsive">
                            <!-- Tab account details -->
                            <div class="tab-pane fade in active" id="tab_account_details">
                                <div class="row">
                                    <div class="avatar-profile-page col-md-3 col-xs-12" id="user_avatar_container">
                                        <span class="img-avatar image" id="user_avatar_thumbnail">
                                            <?php echo get_avatar($user_data->ID, 125) ?>
                                        </span>
                                        <a href="#" id="user_avatar_browse_button">
                                            <?php _e('Change', ET_DOMAIN) ?>
                                        </a>
                                        <span class="et_ajaxnonce hidden" id="<?php echo de_create_nonce( 'user_avatar_et_uploader' ); ?>">
                                        </span>
                                    </div>
                                    <div class="info-profile-page col-md-9 col-xs-12">
                                        <form class="form-info-basic" method="post" id="account_form">
                                            <div class="form-group" style="<?php if($user_role == FREELANCER) { echo 'width:98%'; }?>">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Your Full Name', ET_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="display_name" name="display_name" value="<?php echo $user_data->display_name ?>" placeholder="<?php _e('Enter Full Name', ET_DOMAIN) ?>">
                                                </div>
                                            </div>
											<?php if($user_role != FREELANCER) { ?>
                                            <div class="form-group">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Title', SKILLQUO_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="user_title" name="title" value="<?php echo $user_data->title ?>" placeholder="<?php _e('Enter Title', ET_DOMAIN) ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
											<?php } ?>
											 
                                            <div class="form-group" style="width: 98%;">
                                            	<div class="form-group-control">
                                                	<div class="form-group-control">
                                                        <!--<label><//?php _e('Address', ET_DOMAIN) ?></label>-->
                                                        <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="location" name="location" value="<?php echo $user_data->location ?>" placeholder="<?php _e('Enter address', ET_DOMAIN) ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
															
											<?php if($user_role != FREELANCER) { ?>
                                            <div class="form-group">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('City', SKILLQUO_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="city" name="city" value="<?php echo $user_data->city ?>" placeholder="<?php _e('Enter City', ET_DOMAIN) ?>">
                                                </div>
                                            </div>
											
                                            <div class="form-group">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Postal Code', SKILLQUO_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="postal_code" name="postal_code" value="<?php echo $user_data->postal_code ?>" placeholder="<?php _e('Enter Postal Code', ET_DOMAIN) ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
											
                                            <div class="form-group">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Country', SKILLQUO_DOMAIN) ?></label>-->
                                                   <?php
													$country_arr = array();
													$disabledclass=($under_review)?"disabled":"";
                                                        if(!empty($profile->tax_input['country'])){
                                                            foreach ($profile->tax_input['country'] as $key => $value) {
                                                                $country_arr[] = $value->term_id;
                                                            };
                                                        }
                                                        ae_tax_dropdown( 'country' ,
                                                            array(
                                                                'attr'            => 'data-chosen-disable-search='.$disabledclass.' data-placeholder="'.__("Choose country", ET_DOMAIN).'"',
                                                                'class'           => 'chosen multi-tax-item tax-item required country_profile',
                                                                'hide_empty'      => false,
                                                                'hierarchical'    => true ,
                                                                'value'           => 'slug',
                                                                'id'              => 'country',
                                                                'selected'        => $country,
                                                                'show_option_all' => false
                                                            )
                                                        );
											?>
                                                </div>
                                            </div>
											<?php } ?>
											 			
                                            <div class="form-group" style="<?php if($user_role == FREELANCER) { echo 'width:98%'; }?>">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Email Address', ET_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="email" class="form-control" id="user_email" name="user_email" value="<?php echo $user_data->user_email ?>" placeholder="<?php _e('Enter email', ET_DOMAIN) ?>">
                                                </div>
                                                <?php
                                                    if(!empty($user_meta['newemail'])){
                                                        printf( __( '<p class="noti-update">There is a pending change of the email to %1$s. <a href="%2$s">Cancel</a></p>', ET_DOMAIN ),
                                                                    '<code>' . esc_html( $user_meta['newemail'] ) . '</code>',
                                                                        esc_url( et_get_page_link("profile").'?dismiss=new_email' )
                                                                );
                                                    }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
											
											<?php if($user_role != FREELANCER) { ?>
                                            <div class="form-group"  style="width: 98%;">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Phone', SKILLQUO_DOMAIN) ?></label>-->
                                                    <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="phone" name="phone" value="<?php echo $user_data->phone ?>" placeholder="<?php _e('Enter Phone', ET_DOMAIN) ?>">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
																						
											<?php } ?>
											
                                            <?php if(ae_get_option('use_escrow', false)) {
                                                    do_action( 'ae_escrow_recipient_field');
                                                } ?>
                                            <?php  fre_show_credit( $user_role ) ?>

                                            <?php if($user_role != FREELANCER) { ?>
                                            <h3 style="border-top: 1px solid #ccc; padding: 25px 0;">Company Info</h4>

                                                <div class="form-group" style="width: 98%;">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Business Name', SKILLQUO_DOMAIN) ?></label>-->
                                                        <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="business_name" name="business_name" value="<?php echo $user_data->business_name ?>" placeholder="<?php _e('Enter company name', ET_DOMAIN) ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group" style="width: 98%;">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Brief Description', SKILLQUO_DOMAIN) ?></label>-->
                                                        <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="business_detail" name="business_detail" value="<?php echo $user_data->business_detail ?>" placeholder="<?php _e('Please enter a brief description about your business', ET_DOMAIN) ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Industry', SKILLQUO_DOMAIN) ?></label>-->
                                                        <select <?php if($under_review) echo "disabled"; ?> name="industry">
                                                            <option value="">Select Industry</option>

                                                            <?php

                                                            $industry = array(
                                                                "Aerospace and Defence",
                                                                "Automotive",
                                                                "Construction",
                                                                "CPG",
                                                                "Education/Non Profit",
                                                                "Energy, utilities, Oil and Gas",
                                                                "Finance",
                                                                "Food and Agriculture",
                                                                "Healthcare",
                                                                "Hospitality and Food Service",
                                                                "Information Technology",
                                                                "Management Consulting",
"Manufacturing Operations",
                                                                "Marketing & Brand Management",
"Media and Entertainment",
                                                                "Pharmaceuticals",
                                                                "Retail",
                                                                "Transportation",
"Other",
                                                            );

                                                            foreach($industry as $key => $value) {
                                                                $selected = ( $value == $user_data->industry ) ? "selected" : "";
                                                                echo "<option ".$selected." value='".$value."'>".$value."</option>";
                                                            }

                                                            ?>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Business Type', SKILLQUO_DOMAIN) ?></label>-->
                                                        <select <?php if($under_review) echo "disabled"; ?> name="business_type">
                                                            <option value="">Select Business Type</option>

                                                            <?php

                                                            $business_type = array(
                                                                "Micro",
                                                                "Small",
                                                                "Medium",
                                                                "Large",
                                                            );

                                                            foreach($business_type as $key => $value) {
                                                                $selected = ( $value == $user_data->business_type ) ? "selected" : "";
                                                                echo "<option ".$selected." value='".$value."'>".$value."</option>";
                                                            }

                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group" style="width: 98%;">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Business Website', SKILLQUO_DOMAIN) ?></label>-->
                                                        <input <?php if($under_review) echo "disabled"; ?> type="text" class="form-control" id="business_website" name="business_website" value="<?php echo $user_data->business_website ?>" placeholder="<?php _e('Business Website', ET_DOMAIN) ?>">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <div class="form-group-control">
                                                        <!--<label><//?php _e('Number of Employees', SKILLQUO_DOMAIN) ?></label>-->
                                                        <select <?php if($under_review) echo "disabled"; ?> name="employee_amount">
                                                            <option value="">Select # of Employees</option>

                                                            <?php

                                                            $employee_amount = array(
                                                                //Value    =>  String
                                                                "10orLess" => "&lt;10",
                                                                "10-50" => "10-50",
                                                                "50-100" => "50-100",
                                                                "100orMore" => "&gt;100"
                                                            );

                                                            foreach($employee_amount as $key => $value) {
                                                                $selected = ( $key == $user_data->employee_amount ) ? "selected" : "";
                                                                echo "<option ".$selected." value='".$key."'>".$value."</option>";
                                                            }

                                                            ?>

													</select>
                                                </div>
                                            </div>
											
                                            <div class="form-group">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('How did you hear about us?', SKILLQUO_DOMAIN) ?></label>-->
													<select <?php if($under_review) echo "disabled"; ?> name="hearAboutUs">
													  <option value="">How did you hear about us</option>
													  
													  <?php 
													  
													  $hearAboutUs = array(
														"Friend",
														"Colleague",
														"Event/Networking",
														"Search Engine",
														"Advertisement",
														"Other"
													  );
													  
													  foreach($hearAboutUs as $key => $value) {
														 $selected = ( $value == $user_data->hearAboutUs ) ? "selected" : "";
														 echo "<option ".$selected." value='".$value."'>".$value."</option>";
													  }
													  
													  ?>
													  
													</select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
											
                                            <div class="form-group"  style="width: 98%;">
                                            	<div class="form-group-control">
                                                    <!--<label><//?php _e('Business Detail', SKILLQUO_DOMAIN) ?></label>-->
													<br/>
													<textarea <?php if($under_review) echo "disabled"; ?> rows="4" style="width: 100%;" name="business_description" placeholder="Provide additional business details"><?php echo $user_data->business_description;?></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
											<?php } ?>
											<?php if(!$under_review) { ?>
                                            <div class="form-group">
                                               <input id="specialButton" type="submit" class="btn-submit btn-sumary" name="" value="<?php _e('Save', ET_DOMAIN) ?>">
                                            </div>
											<?php } ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--// END ACCOUNT DETAILS -->
                            <!-- Tab profile details -->
                            <?php if(fre_share_role() || $user_role == FREELANCER) { ?>
                            <div class="tab-pane fade" id="tab_profile_details">
                            	<div class="detail-profile-page">
                                    <?php if(isset($_GET['loginfirst']) && $_GET['loginfirst'] == 'true'){ ?>
                                        <div class="notice-first-login">
                                            <p><?php _e('<i class="fa fa-warning"></i>You must complete your profile to do any activities on site', ET_DOMAIN);?></p>
                                        </div>
                                    <?php } ?>
                                	<form class="form-detail-profile-page" method="post" id="profile_form">
										<div class="clearfix"></div>
                                        <div class="form-group">
                                        	<div class="form-group-control">
                                                <label>* <?php _e('Professional Title', ET_DOMAIN) ?></label>
                                                <input <?php if($under_review) echo "disabled"; ?> class="input-item form-control text-field"  type="text" name="et_professional_title"
                                                <?php   if($job_title){
                                                            echo 'value= "'.esc_attr($job_title).'" ';
                                                        }?>
                                                       placeholder="<?php _e("e.g:V.P. Supply Chain", ET_DOMAIN) ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    	<div class="form-group">
                                        	<div class="form-group-control hourly-rate">
                                                <label>* <?php _e('Your Hourly Rate', ET_DOMAIN) ?></label>
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <input <?php if($under_review) echo "disabled"; ?> class="input-item form-control text-field"  type="text" name="hour_rate"
                                                               <?php   if($hour_rate){
                                                            echo "value= $hour_rate ";
                                                        }?>  placeholder="<?php _e('e.g:120', ET_DOMAIN) ?>">
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <span class="profile-exp-year">
                                                        <?php $currency = ae_get_option('content_currency');
                                                        if($currency){
                                                            echo $currency['code'];
                                                        }else{
                                                            _e('USD', ET_DOMAIN);
                                                        } ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="skill-profile-control form-group-control">
                                             <label>*<?php _e('Your Skills (max 30)', ET_DOMAIN) ?></label>
                                                    <label>Please type your skills/areas of expertise, one at a time, and press ENTER to add</label>
                                                <?php
                                                $switch_skill = ae_get_option('switch_skill');
                                                if(!$switch_skill){
                                                    ?>
                                                    <input class="form-control skill" type="text" id="skill" name="skill" placeholder="<?php sprintf(__("Skills (max is %s)", ET_DOMAIN), ae_get_option('fre_max_skill', 30)); ?>" autocomplete="off" spellcheck="false" >
                                                    <ul class="skills-list" id="skills_list"></ul>
                                                    <?php
                                                } else {
                                                    $c_skills = array();
													
														if(!empty($current_skills)){
															foreach ($current_skills as $key => $value) {
																$c_skills[] = $value->term_id;
															};
														}
														ae_tax_dropdown( 'skill' , array(  'attr' => 'data-chosen-width="100%" required data-chosen-disable-search="" multiple data-placeholder="'.sprintf(__(" Skills (max is %s)", ET_DOMAIN), ae_get_option('fre_max_skill', 5)).'"',
																			'class'             => 'sw_skill required',
																			'hide_empty'        => false,
																			'hierarchical'      => false ,
																			'id'                => 'skill' ,
																			'show_option_all'   => false,
																			'selected'          => $c_skills
																			)
														);
                                                }

                                                ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="profile-category">
                                                <label><?php _e('Category', ET_DOMAIN) ?></label>
                                                <?php
                                                    $cate_arr = array();
                                                    if(!empty($profile->tax_input['project_category'])){												
                                                        foreach ($profile->tax_input['project_category'] as $key => $value) {
                                                            $cate_arr[] = $value->term_id;
                                                        };
                                                    }
                                                    ae_tax_dropdown( 'project_category' ,
                                                          array(
                                                                'attr'            => 'data-chosen-width="100%" multiple="multiple" data-chosen-disable-search="" data-placeholder="'.__("Choose categories", ET_DOMAIN).'"',
                                                                'class'           => 'chosen multi-tax-item tax-item required cat_profile',
                                                                'hide_empty'      => false,
                                                                'hierarchical'    => false ,
                                                                'id'              => 'project_category' ,
                                                                'selected'        => $cate_arr,
                                                                'show_option_all' => false
                                                              )
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
										<?php if(!$under_review) { ?>
											<?php if(fre_share_role() || $user_role == FREELANCER){?>
											<div class="form-group">
												<div class="form-group-control">
													<label class="et-receive-mail" for="et_receive_mail"><input type="checkbox" id="et_receive_mail" name="et_receive_mail_check" <?php echo (isset($profile->et_receive_mail) && $profile->et_receive_mail == '1') ? 'checked': '' ;?>/><?php _e('Receive emails about projects that match your categories', ET_DOMAIN) ?></label>
													<input <?php if($under_review) echo "disabled"; ?> class="input-item form-control text-field"  type="hidden" value="<?php echo (isset($profile->et_receive_mail)) ? $profile->et_receive_mail : '';?>" id="et_receive_mail_value" name="et_receive_mail" />
												</div>
											</div>
											<div class="clearfix"></div>
											<?php } ?>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="form-group-control">
                                                <label><?php _e('Your Country', ET_DOMAIN) ?></label>
                                                <?php if(!ae_get_option('switch_country')){ ?>
                                                        <input <?php if($under_review) echo "disabled"; ?> required class="input-item form-control text-field" type="text" id="country" placeholder="<?php _e("Country", ET_DOMAIN); ?>" name="country"
                                                               <?php if($country){ echo "value='".$country."'"; } ?> autocomplete="off" class="country" spellcheck="false" >
                                                <?php }else{
                                                        $country_arr = array();
                                                        if(!empty($profile->tax_input['country'])){
                                                            foreach ($profile->tax_input['country'] as $key => $value) {
                                                                $country_arr[] = $value->term_id;
                                                            };
                                                        }
                                                        if(empty($country_arr)) $country_arr = $country;
														ae_tax_dropdown( 'country' ,
                                                            array(
                                                                'attr'            => 'data-chosen-disable-search="" data-placeholder="'.__("Choose country", ET_DOMAIN).'"',
                                                                'class'           => 'chosen multi-tax-item tax-item required country_profile',
                                                                'hide_empty'      => false,
                                                                'hierarchical'    => true ,
                                                                'value'           => 'slug',
                                                                'id'              => 'country' ,
                                                                'selected'        => $country_arr,
                                                                'show_option_all' => false
                                                            )
                                                        );
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group about-you">
                                        	<div class="form-group-control row-about-you">
                                                <label><?php _e('About you', ET_DOMAIN) ?></label>
                                                <div class="clearfix"></div>
                                                <?php if(!$under_review) { ?> 
													<textarea class="form-control" name="post_content" id="about_content" cols="30" rows="5"><?php echo esc_textarea(trim($about)) ?></textarea>
												<?php } else { ?>
													<?php echo trim($about) ?>
												<?php } ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                        	<div class="experience">
                                                <label>* <?php _e('Your Experience', ET_DOMAIN) ?></label>
                                                <div class="row">
                                                    <div class="col-md-12 fix-width">
                                                        <input <?php if($under_review) echo "disabled"; ?> class="form-control col-xs-3 number is_number numberVal" step="1" min="1" type="number" name="et_experience" value="<?php echo $experience; ?>" />
                                                        <span class="col-xs-3 profile-exp-year"><?php _e("year(s)", ET_DOMAIN); ?></span>
                                                    </div>
                                                    <div class="col-xs-3">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
										<?php if(fre_share_role() || $user_role == FREELANCER){ ?>
										<div class="info-project-items past-experience-container">
											<h4 class="title-big-info-project-items" style="padding-left: 0;">Work Experience</h4>
											<ul class="bid-list-container" id="experience-list-container">
											
												<?php 
												
												$titles = unserialize($exp_title);
												$employer = unserialize($exp_employer);
												$toDate = unserialize($exp_toDate);
												$fromDate = unserialize($exp_fromDate);
												$description = unserialize($exp_description);
												$stillWorking = unserialize($exp_stillWork);
												$experienceCount = count($titles);
												
												for($i = 0; $i < $experienceCount; $i++) {
													if($titles[$i]) {
														echo "<li id='exp_list_".$i."'>";
															if(!$under_review) { echo '<span data-featherlight="#work_'.$i.'" class="btn-edit btn-sumary">Edit</span>'; }
															echo '<span class="title">'.$titles[$i].'</span> at ';
															echo '<span class="employer">'.$employer[$i].'</span>';
															
															if(!$toDate[$i])
																echo '<span class="date">'.$fromDate[$i].'</span>';
															else
																echo '<span class="date">'.$fromDate[$i].' - '.$toDate[$i].'</span>';
														
															echo '<p>'.$description[$i].'</p>';
														echo "</li>";
													}
												}
												
												?>
										
											</ul>
											<?php if(!$under_review) { ?><span data-featherlight="#emptyExp" class="btn-sumary btn-add">Add</span><?php } ?>
										</div>
										<div class="info-project-items past-experience-container">
											<h4 class="title-big-info-project-items" style="padding-left: 0;">Education</h4>
											<ul class="bid-list-container" id="education-list-container">
												<?php 
												$schools = unserialize($education_school);
												$focus = unserialize($education_focus);
												$year = unserialize($education_year);
												$degree = unserialize($education_degree);
												$educationCount = count($schools);
												
												for($i = 0; $i < $educationCount; $i++) {
													if($schools[$i]) {
														echo "<li id='edu_list_".$i."' class='education'>";
															if(!$under_review) { echo '<span data-featherlight="#school_'.$i.'" class="btn-edit btn-sumary">Edit</span>'; }
															echo '<span class="title">'.$schools[$i].'</span><br/>';
															echo '<span class="employer">'.$degree[$i].'</span>';
															echo '<span class="focus">'.$focus[$i].'</span>';
															
															$attended_years = explode(",",$year[$i]);
															if($attended_years[0] && $attended_years[1]) 
																echo '<span class="date">'.$attended_years[0] .' - '. $attended_years[1].'</span>';
															else if($attended_years[0])
																echo '<span class="date">'.$attended_years[0].'</span>';
															
														echo "</li>";
													}
												}
												
												?>
											</ul>
											<?php if(!$under_review) { ?><span data-featherlight="#emptyEducation" class="btn-sumary btn-add">Add</span><?php } ?>
										</div>
										<div class="clearfix"></div>
										<?php } ?>
                                        <!--// project description -->
                                        <?php do_action( 'ae_edit_post_form', PROFILE, $profile ); ?>
										
										<?php if(!$under_review && $hide) { ?>
                                        <div class="form-group portfolios-wrapper">
                                        	<div class="form-group-control">
                                                <label><?php _e('Your Portfolio', ET_DOMAIN) ?></label>
                                                <div class="edit-portfolio-container">
                                                    <?php
                                                    // list portfolio
                                                    query_posts( array(
                                                        'post_status' => 'publish',
                                                        'post_type'   => 'portfolio',
                                                        'author'      => $current_user->ID,
                                                        'posts_per_page' => -1
                                                    ));
                                                    get_template_part( 'list', 'portfolios' );
                                                    wp_reset_query();
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
										<?php } ?>
                                        <div class="clearfix"></div>
										<?php if(!$under_review) { ?>
                                        <div class="form-group">
                                        	<input id="saveProfile" type="submit" class="btn-submit btn-sumary" name="" value="<?php _e('Save', ET_DOMAIN) ?>">
											<script>
												jQuery("#saveProfile").click(function(){
													
													if(jQuery("#submit_error").length){
														jQuery("#submit_error").slideUp("fast",function(){
															jQuery("#submit_error").remove();
														});
													}
													
													if(jQuery("input[name='et_professional_title']").val().trim() == "") {
														jQuery("<p id='submit_error' style='display: none;'>There was an error. You need to have a Professional Title</p>").appendTo("#tab_profile_details").slideDown();
													} else if(jQuery("input[name='hour_rate']").val().trim() == "") {
														jQuery("<p id='submit_error' style='display: none;'>There was an error. You need to have a hourly rate</p>").appendTo("#tab_profile_details").slideDown();
													} else if(jQuery("input[name='et_experience']").val().trim() == "") {
														jQuery("<p id='submit_error' style='display: none;'>There was an error. You need to fill out your years of experience</p>").appendTo("#tab_profile_details").slideDown();
													} else if(jQuery(".confirm-request").length >= 1){
														jQuery("<p id='submit_error' style='display: none;'>You must confirm your account before you can do anything.</p>").appendTo("#tab_profile_details").slideDown();
													} else {
														setTimeout(function(){document.getElementById("profile_form").submit();},2000);
													}
													

												});
											</script>
                                        </div>
										<?php } ?>
                                    </form>
                                </div>
                            </div>
                            <?php } ?>
                            <!--// END PROFILE DETAILS -->
                            <!-- tab project details -->
                            <div class="tab-pane fade" id="tab_project_details">
                                <?php
                                // list all freelancer current bid
                                if(fre_share_role() || $user_role == FREELANCER){ ?>
                            	<div class="info-project-items">
                                    <h4 class="title-big-info-project-items">
                                        <?php _e("Current bids", ET_DOMAIN) ?>
                                    </h4>
                                    <?php
                                        query_posts( array(
                                            'post_status' => array('publish', 'accept' ),
                                            'post_type'   => BID,
                                            'author'      => $current_user->ID,
                                        ));

                                        get_template_part( 'list', 'user-bids' );

                                        wp_reset_query();
                                    ?>
                                </div>
                                <?php
                                }

                                if(fre_share_role() || $user_role != FREELANCER) {
                                    // employer works history & reviews
                                    get_template_part('template/work', 'history');
                                }

                                if(fre_share_role() || $user_role == FREELANCER) {
                                    // freelancer bids history and reviews
                                    get_template_part('template/bid', 'history');
                                }
                                ?>
                            </div>
                            <?php do_action('fre_profile_tab_content');?>
                            <!--// END PROJECT DETAILS -->

                            <!-- tab inbox-->
                            <div class="tab-pane fade" id="tab_inbox">
                                <div class="row">
                                <?php  echo do_shortcode('[RC_Inbox]');?>
                                </div>
                            </div>
                           <!--// END INBOX -->

                            <!--End show model-->
                            <!--// END TABS CREDITS-->
                        </div>
                    </div>
                </div>
                <!-- profile left bar -->
                <div class="col-md-4">
                	<div class="setting-profile-wrapper <?php echo $user_role; ?>">
						<?php if($under_review && !$permaban){ ?>
						<div class="profile-under-review">
							Your Profile is: <strong>Under Review</strong>.
						</div>
						<?php } ?>
						<?php if($status == 1){ ?>
						<div class="profile-approve-review">
							Your Profile is: <strong>Approved</strong>!
						</div>
						<?php } ?>
						<?php if($status == 2 && !$permaban){ ?>
						<div class="profile-rejected-review">
							Your Profile was: <strong>Rejected</strong>!
						</div>
						<?php } ?>
						<?php if($permaban){ ?>
						<div class="profile-rejected-review">
							<strong>Permanently Rejected</strong>
						</div>
						<?php } ?>
						<div id="completion_module">
							<h3>Profile Completion</h3>
							<div class="completion_bar_component">
							<?php //Calculate to 100% 
							if($user_role == FREELANCER){ 
								$profileItems = array(
									"photo" 		=>	"Photo",
									"hourly"		=>  "Hourly Rate",
									"years_of_exp"  =>  "Years of Experience",
									"tagline" 		=>	"Professional Title",
									"about" 		=>	"About Me",
									"employer" 		=>	"Experiences",
									"education" 	=>	"Education",
								);

								$item["photo"] = get_avatar($user_ID);
								
								if(strpos($item["photo"],"gravatar") !== false) {
									unset($item["photo"]);
								}
								
								$item["email"] = 1;
								
								$item["tagline"] = get_post_meta($profile_id,"et_professional_title",true);
								
								$item["about"] = $about;
								$item["country"] = $country;
								$item["category"] = $category;
								$item["current_skills"] = $current_skills;
								$item["skills"] = $skills;
								$item["hour_rate"] = $hour_rate;
								$item["years_of_exp"] = $experience;
								
								$item["education"] = $schools[0];
								$item["employer"] = $employer[0];	
								$item["experience"] = $experience;
								$item["hourly"] = $hour_rate;			
								
								$item["display_name"] = true;
								
								$icon["photo"] = "fa-times";
								$icon["tagline"] = "fa-times";
								$icon["about"] = "fa-times";
								$icon["employer"] = "fa-times";
								$icon["education"] = "fa-times";
								$class["photo"] = "fail";
								$class["tagline"] = "fail";
								$class["about"] = "fail";
								$class["employer"] = "fail";
								$class["education"] = "fail";
								
								$icon["hourly"] = "fa-times";
								$class["hourly"] = "fail";
								
								$icon["years_of_exp"] = "fa-times";
								$class["years_of_exp"] = "fail";
								
								$icon["tagline"] = "fa-times";
								$class["tagline"] = "fail";
								
								foreach($item as $key => $check) {
									if(isset($item[$key]) && $item[$key] != "") {
										$profile_value = $profile_value + 1;
										
										$icon[$key] = "fa-check";
										$class[$key] = "success";
										
									}
								}

								$completion_value = ($profile_value / 15) * 100;
								
							} else {
								$items["photo"] = get_user_meta($user_ID,"et_avatar_url",true);
																
								$items["name"] 							= get_user_meta($user_ID,"first_name",true);
								
								$items["contact"]["email"] 				= "true";
								$items["contact"]["address"] 			= get_user_meta($user_ID,"location",true);
								$items["contact"]["postal_code"] 		= get_user_meta($user_ID,"postal_code",true); 
								$items["contact"]["city"] 				= get_user_meta($user_ID,"city",true); 
								$items["contact"]["country"] 			= get_user_meta($user_ID,"country",true); 
								$items["contact"]["phone"] 				= get_user_meta($user_ID,"phone",true);
								
								$items["company"]["name"] 				= get_user_meta($user_ID,"business_name",true);
								$items["company"]["detail"] 			= get_user_meta($user_ID,"business_detail",true);
								$items["company"]["description"] 		= get_user_meta($user_ID,"business_description",true);
								$items["company"]["website"] 			= get_user_meta($user_ID,"business_website",true);
								$items["company"]["type"] 				= get_user_meta($user_ID,"business_type",true);
								$items["company"]["employee_amount"] 	= get_user_meta($user_ID,"employee_amount",true);
								$items["company"]["industry"] 			= get_user_meta($user_ID,"industry",true);
								
								$icon["photo"] = "fa-times";
								$icon["contact"] = "fa-times";
								$icon["business"] = "fa-times";
								$class["business"] = "fail";
								$class["contact"] = "fail";
								$class["photo"] = "fail";
								
								foreach($items["contact"] as $key => $contact){
									if(isset($items["contact"][$key]) && $items["contact"][$key] != "")
										$contact_value = $contact_value + 1;
								}
								
								$contact_total = ($contact_value / 6) * 33;
								
								if($contact_total >= 33) {
									$icon["contact"] = "fa-check";
									$class["contact"] = "success";
								}
								
								foreach($items["company"] as $key => $company){
									if(isset($items["company"][$key]) && $items["company"][$key] != "")
										$company_value = $company_value + 1;
								}
								
								$company_total = ($company_value / 7) * 33;

								if($company_total >= 33) {
									$icon["business"] = "fa-check";
									$class["business"] = "success";
								}
								
								if($items["photo"]) {
									$completion_value = $completion_value + 34;
									$icon["photo"] = "fa-check";
									$class["photo"] = "success";
								}
								$completion_value = $company_total + $completion_value + $contact_total;
								
								$profileItems = array(
									"photo" 		=>	"Photo",
									"contact" 		=>	"Contact Info",
									"business" 		=>	"Company Detail"
								);
								
								
							}
							
								if($completion_value <= 40) {
									$color = "red";
								} else if($completion_value <= 60) {
									$color = "orange";
								} else if($completion_value <= 75) {
									$color = "yellow";
								} else if($completion_value <= 90) {
									$color = "light-green";
								} else if($completion_value <= 110) {
									$color = "green";
								}
							
							
							?>
								<span><?php echo number_format($completion_value,2); ?>%</span>
								<div class="completion_bar">
									<div class="<?php echo $color; ?>" style="height: 3px; width: <?php echo $completion_value; ?>%;"></div>
								</div>
							</div>
							
							<?php 

							echo "<div class='completion_list'>";
							foreach($profileItems as $key => $item){
								echo "<span class='".$class[$key]."'><i class=\"fa ".$icon[$key]."\"></i> ".$item."</span>";
							}
							echo "</div>";
							
							?>
						</div>
						<?php if(!$permaban && $status != 1){ ?>
							<?php if(!$under_review) { ?>
							<div class="pdf-group">
								<form style="display: none;" id="submitForReview_form" method="post">
									<input type="hidden" name="submitForReview" value="1">
									<input type="hidden" name="review"  value="<?php if($status == 2) echo "2"; else echo "1"; ?>">
									<input type="submit" name="review_submit">
								</form>
								Once you have reach 100% Profile Completion, you can submit your profile for review.
								<?php if($completion_value >= 100) { ?>
									<span data-featherlight="#warningReview" class="btn-submit btn-sumary btn-linkOrange">Submit for Review</span>
								<?php } else { ?>
									<span id="profileNot100" class="btn-submit btn-sumary btn-linkGrey">Submit for Review</span>
								<?php } ?>
							</div>
							<?php } ?>
							
						<?php } ?>
                        <?php if($user_role == FREELANCER){ ?>
							<?php if(!$under_review) { ?>
							<div class="pdf-group">
								<form style="display: none;" name="pdfFormSubmitter" method="post" id="pdfFormSubmitter" enctype="multipart/form-data">
									Upload your LinkedIn PDF:
									<input type="file" name="fileToUpload" id="fileToUpload">
									<input type="submit" value="Continue" name="uploadPDF">
								</form>
								Looking to quickly add items to your profile? 
								<br/> You can import your LinkedIn resume.
								<a target="_blank" href="https://www.skillquo.com/linkedin-profile.php"><span class="btn-sumary btn-linkHOW">Learn how</span></a>
								<span data-featherlight="#warningPDF" class="btn-submit btn-sumary btn-linkPDF">Upload your LinkedIn PDF</span>
								<script>
									jQuery("#pdfFormSubmitter").on( 'change', '#fileToUpload' , function () {
										jQuery(".featherlight-content #warningPDF").slideUp(500,function(){
											jQuery(".featherlight-content #warningPDF").html("Your PDF is now being Analyzed <img src='/wp-content/themes/freelanceengine/includes/aecore/assets/img//loading.gif' />").slideDown(1000);
										});
										document.getElementById('pdfFormSubmitter').submit();
									});
								</script>
							</div>
							<div class="form-group">
								<span class="text-intro">
									<?php _e("Available for hire?", ET_DOMAIN) ?></span>
								<span class="switch-for-hide tooltip-style" data-toggle="tooltip" data-placement="top"
									title='<?php _e('Turn on to display an "Invite me" button on your profile, allowing potential employers to suggest projects for you.', ET_DOMAIN);  ?>'
								>
									<input <?php if($under_review) echo "disabled"; ?> type="checkbox" <?php echo $user_available; ?> class="js-switch user-available" name="user_available"/>
									<span class="user-status-text text <?php echo $user_available ? 'yes' : 'no' ?>">
										<?php echo $user_available ? __('Yes', ET_DOMAIN) : __('No', ET_DOMAIN); ?>
									</span>
								</span>
							</div>
							<div class="clearfix"></div>
							<div class="form-group">
								<span class="text-small">
									<?php _e('Select "Yes" to display a "Hire me" button on your profile allowing potential clients and employers to contact you.', ET_DOMAIN) ?>
								</span>
							</div>
							<?php 
							}
						}
                        // display a link for user to request a confirm email
                        if( !AE_Users::is_activate($user_ID) ) {
                         ?>

                        <div class="form-group confirm-request">
                            <span class="text-small">
                                <?php
                                _e('You have not confirmed your email yet, please check out your mailbox.', ET_DOMAIN);
                                echo '<br/>';
                                echo ' <a class="request-confirm" href="#">' .__( 'Request confirm email.' , ET_DOMAIN ). '</a>';
                                 ?>
                            </span>
                        </div>
                        <?php } ?>
                        <ul class="list-setting">
                            <?php if(fre_share_role() || $user_role != FREELANCER ) { ?>
							<?php if($status == 1) { ?>
                            <li>
                                <a role="menuitem" tabindex="-1" href="<?php echo et_get_page_link("submit-project") ?>" class="display-name">
                                    <i class="fa fa-plus-circle"></i><?php _e("Post a Project", ET_DOMAIN) ?>
                                </a>
                            </li>
							<?php } ?>
                            <?php } ?>
                        	<li>
                                <a href="#" class="change-password">
                                    <i class="fa fa-key"></i>
                                    <?php _e("Change Password", ET_DOMAIN) ?>
                                </a>
                            </li>
                            <?php do_action('fre-profile-after-list-setting');?>
                            <?php if(ae_get_option('use_escrow', false)) {
                                do_action( 'ae_escrow_stripe_user_field');
                            } ?>
                            <!-- <li>
                                <a href="#" class="creat-team-link"><i class="fa fa-users"></i><?php _e("Create Your Team", ET_DOMAIN) ?></a>
                            </li> -->
                            <li>
                                <a href="<?php echo wp_logout_url( home_url() ); ?>" class="logout-link">
                                    <i class="fa fa-sign-out"></i>
                                    <?php _e("Log Out", ET_DOMAIN) ?>
                                </a>
                            </li>
                              <!-- HTML to write -->
                        </ul>
                        <div class="widget user_payment_status">
                        <?php ae_user_package_info($user_ID); ?>
                        </div>
                    </div>
					<div class="row">
                        <?php /////////
						//////////////////  Favourite Freelancers Box starts   (devTooba)
						$liked_users = get_user_meta(get_current_user_id(), '__ids_users_liked', true);
						if(ae_user_role(get_current_user_id()) != FREELANCER && $liked_users){
						?>
                        <div class="col-md-12">
							<div class="meet-qou">
								<div class="white-bx">
									<div class="title-hd"><h4>My Favourites<div class="mtqou-contolers-custom"><i class="fa fa-angle-left" aria-hidden="true"></i><i class="fa fa-angle-right" aria-hidden="true"></i></div></h4></div>
									<div class="bx-inner" style="padding: 0px;overflow: hidden;">
										<div class="m-quo-slider-custom">
											<ul>
												<?php 
		                               				$count=0;
		                                			 if($liked_users) {
		                                       			 $postdata = array();
				                                        foreach($liked_users as $liked_user_id) { 
														$rating = Fre_Review::freelancer_rating_score($liked_user_id);//print_r(get_userdata($liked_user_id)->user_url);
				                                        	if($count%2==0 && $count!=0)
				                                        		echo '<div style="clear:both;"></div></ul><ul><li><a href="'.get_author_posts_url($liked_user_id).'"><span class="user-small-dp"><span class="original-dp">'.get_avatar($liked_user_id).'</span></span>'.get_userdata($liked_user_id)->display_name.'<div class="rate-it" data-score="'.$rating['rating_score'].'"></a></li>'  ;
				                                        	else 
				                                            	echo '<li><a href="'.get_author_posts_url($liked_user_id).'"><span class="user-small-dp"><span class="original-dp">'.get_avatar($liked_user_id).'</span></span>'.get_userdata($liked_user_id)->display_name.'<div class="rate-it" data-score="'.$rating['rating_score'].'"></div></a></li>';
																$count++;		                                            }
				                                    }
		                                			?>
											<div style="clear:both;"></div></ul>
										</div>
                                        
									</div>
								</div>
							</div>
						</div>
                        
                         <?php 
						}
						 /////////
						//////////////////  Favourite Freelancers Box end 
						?>
					</div>
                </div>
                <!--// profile left bar -->
            </div>
        </div>
    </div>

</section>

<!-- CURRENT PROFILE -->
<?php if($profile_id && $profile_post && !is_wp_error( $profile_post )){ ?>
<script type="data/json" id="current_profile">
    <?php echo json_encode($profile) ?>
</script>
<?php } ?>
<!-- END / CURRENT PROFILE -->

<!-- CURRENT SKILLS -->
<?php if( !empty($current_skills) ){ ?>
	<script type="data/json" id="current_skills">
		<?php echo json_encode($current_skills) ?>
	</script>
<?php } ?>

<!-- END / CURRENT SKILLS -->
<?php if($user_role == FREELANCER ) { ?>
<div class="hide">
<?php 

		for($i = 0; $i <= $experienceCount; $i++) {
			
			if($under_review) break;
			
			if($titles[$i] || ($i == $experienceCount)) {
				
				if($i != $experienceCount) {
					echo '<div id="work_'.$i.'">';
				} else {
					echo '<div id="clone_work">';
				}
					?>
					
					<form style="width: 540px;" lass="form-horizontal" method="post">
						<fieldset>
						<input type="hidden" value="<?php echo $i; ?>" name="experience" />
						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_employer">Employer</label>  
						  <div class="col-md-8">
							<input name="exp_employer" type="text" placeholder="Employer" value="<?php echo $employer[$i] ?>" class="form-control input-md" required="">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_title">Title</label>  
						  <div class="col-md-8">
							<input name="exp_title" type="text" placeholder="Title" value="<?php echo $titles[$i] ?>" class="form-control input-md" required="">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_fromDate">Start Date</label>  
						  <div class="col-md-8">
							<input name="exp_fromDate" type="text" placeholder="Start Date" value="<?php echo $fromDate[$i] ?>" class="form-control input-md">
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_toDate">Last Date</label>  
						  <div class="col-md-8">
							<input name="exp_toDate" type="text" placeholder="Last Date" value="<?php echo $toDate[$i] ?>" class="form-control input-md">
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_description">Job Summary</label>
						  <div class="col-md-8">                     
							<textarea class="form-control" rows="10" name="exp_description"><?php echo str_replace("<br />","",$description[$i]); ?></textarea>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label" for="exp_stillWork"></label>
						  <div class="col-md-8">
							<label class="checkbox-inline" for="exp_stillWork-0">
							  <input type="checkbox" name="exp_stillWork" <?php if($stillWorking[$i]) echo "checked";  ?> value="1">
							  Still work here?
							</label>
						  </div>
						</div>

						<!-- Button -->
						<div class="form-group text-right">
						  <div class="col-md-12">
							<a href="#" onclick="delete_exp('<?php echo $i; ?>')" name="action" value="delete" style="color: #ff9898; padding: 5px;" name="action">Delete</a>
							<button type="button" onclick="jQuery.featherlight.close()" name="close" class="btn btn-cancel" value="cancel" name="action">Cancel</button>
							<button type="button" onclick="update_exp('<?php echo $i; ?>')" value="save" name="action" class="btn btn-success">Save</button>
						  </div>
						</div>

						</fieldset>
					</form>

					<?php
				echo "</div>";
			}
		}
		
		for($i = 0; $i <= $educationCount; $i++) {
			
			if($under_review) break;
			
			if($schools[$i] || ($i == $educationCount)) {
				
				if($i != $educationCount)
					echo '<div id="school_'.$i.'">';
				else 
					echo '<div id="clone_school">';
				?>
					<form style="width: 420px;" class="form-horizontal" method="post">
						<fieldset>
						<input type="hidden" value="<?php echo $i; ?>" name="education" />
						<!-- Text input-->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="education_school">School Name</label>  
						  <div class="col-md-8">
						  <input value="<?php echo $schools[$i] ?>" name="education_school" type="text" placeholder="What school did you go to?" class="form-control input-md" required="">
							
						  </div>
						</div>

						<!-- Text input-->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="education_focus">Major</label>  
						  <div class="col-md-8">
						  <input value="<?php echo $focus[$i] ?>"  name="education_focus" type="text" placeholder="What did you focus on?" class="form-control input-md">
							
						  </div>
						</div>

						<!-- Text input-->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="education_degree">Degree</label>  
						  <div class="col-md-8">
						  <input value="<?php echo $degree[$i] ?>"  name="education_degree" type="text" placeholder="What degree were you working on?" class="form-control input-md">
							
						  </div>
						</div>

						<!-- Text input-->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="education_year">Attended Date</label>  
						  <div class="col-md-8">
						  
						  <?php $attended_date = explode(",",$year[$i]); ?>
						  
						  <select name="attend_1">
						  <?php unset($date); ?>
						  <?php $year_loop = date("Y"); ?>
						  <?php if(!$attended_date[0]) $attended_date[0] = date("Y"); ?>
						  <?php for($yeari = 1900; $yeari <= $year_loop; $yeari++) {
							if($attended_date[0] == $yeari)
								$date =  "<option selected value='".$yeari."'>".$yeari."</option>".$date;
							else
								$date =  "<option value='".$yeari."'>".$yeari."</option>".$date;
							
							
						  } ?>
						  <?php echo $date;?>
						  </select>
						  <span> - </span>
						  <select name="attend_2">
						  <?php unset($date); ?>
						  <?php if(!$attended_date[1]) $attended_date[1] = date("Y"); ?>
						  <?php for($yeari = 1900; $yeari <= ($year_loop+7); $yeari++) {
							if($attended_date[1] == $yeari)
								$date =  "<option selected value='".$yeari."'>".$yeari."</option>".$date;
							else
								$date =  "<option value='".$yeari."'>".$yeari."</option>".$date;
							
						  } ?>
						  <?php echo $date;?>
						  </select>

						  </div>
						</div>

						<!-- Button -->
						<div class="form-group text-right">
						  <div class="col-md-12">
							<a href="#" onclick="delete_edu('<?php echo $i; ?>')" name="action" value="delete" style="color: #ff9898; padding: 5px;" name="action">Delete</a>
							<button type="button" onclick="jQuery.featherlight.close()" name="close" class="btn btn-cancel" value="cancel" name="action">Cancel</button>
							<button type="button" onclick="update_edu('<?php echo $i; ?>')" value="save" name="action" class="btn btn-success">Save</button>
						  </div>
						</div>

						</fieldset>
					</form>
				
				<?php
				echo '</div>';
			}
		}
		
?>
<?php if(!$under_review) { ?>
	<div id="emptyExp">
		<form style="width: 540px;" class="form-horizontal" method="post">
			<fieldset>
				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_employer">Employer</label>  
					<div class="col-md-8">
					<input name="exp_employer" type="text" placeholder="Employer" class="form-control input-md" required="">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_title">Title</label>  
					<div class="col-md-8">
					<input name="exp_title" type="text" placeholder="Title" class="form-control input-md" required="">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_fromDate">Start Date</label>  
					<div class="col-md-8">
					<input name="exp_fromDate" type="text" placeholder="Start Date" class="form-control input-md">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_toDate">Last Date</label>  
					<div class="col-md-8">
					<input name="exp_toDate" type="text" placeholder="Last Date" class="form-control input-md">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_description">Job Summary</label>
					<div class="col-md-8">                     
					<textarea class="form-control" name="exp_description" rows="10" placeholder="What did you do here?"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="exp_stillWork"></label>
					<div class="col-md-8">
					<label class="checkbox-inline" for="exp_stillWork-0">
					<input type="checkbox" name="exp_stillWork" value="1">
					Still work here?
					</label>
					</div>
				</div>

				<div class="form-group text-right">
				  <div class="col-md-12">
					<button type="button" onclick="jQuery.featherlight.close(); " name="close" class="btn btn-cancel" value="cancel" name="action">Cancel</button>
					<button type="button" onclick="emptyExp()" name="action" value="add" class="btn btn-primary">Add</button>
				  </div>
				</div>

				<input type="hidden" value="<?php echo $experienceCount+1; ?>" name="experience" />
				
			</fieldset>
		</form>
		<script>
			<?php 
			//Ready for hell? 
			//Empty means "empty form".
			?>
			function nl2br (str, is_xhtml) {
				var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
				return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
			}
						
			function emptyExp(){
				event.preventDefault();
				
				var checked;
				if(jQuery('.featherlight-content #emptyExp form input[name=exp_stillWork]').is(':checked')) {
					checked = "1";
				} else {
					checked = "0";
				}
				
				var form = {
					action: "exp_update", 
					crud: "add",
					exp_employer: jQuery(".featherlight-content #emptyExp form input[name=exp_employer]").val(),
					exp_title:  jQuery(".featherlight-content #emptyExp form input[name=exp_title]").val(),
					exp_toDate:  jQuery(".featherlight-content #emptyExp form input[name=exp_toDate]").val(),
					exp_fromDate:  jQuery(".featherlight-content #emptyExp form input[name=exp_fromDate]").val(),
					exp_stillWork:  checked,
					exp_description: jQuery(".featherlight-content #emptyExp form [name=exp_description]").val(),
					experience: jQuery(".featherlight-content #emptyExp form input[name=experience]").val()
				};
				
				jQuery(".featherlight-content #emptyExp form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #emptyExp'))
						.slideDown("fast");
				});
				
				jQuery.post(ajaxurl, form, function(response) {
					if(response == "success") {
						
						var div = jQuery('div[id^="work_"]:last');
						
						if (div.size() == '1') {
							var num = parseInt( div.prop("id").match(/\d+/g), 10 ) +1;
							var clone = div.clone().prop('id', 'work_'+num );
						} else {
							var div = jQuery('#clone_work');
							var num = "0";
							var clone = div.clone().prop('id', 'work_'+num );
						}
						
						var clone_element = jQuery('<li id="exp_list_3"><span data-featherlight="#work_3" class="btn-edit btn-sumary">Edit</span><span class="title">Attorney</span> at <span class="employer">Cleary Gottlieb Steen &amp; Hamilton LLP</span><span class="date">1994 - 2001</span><p></p></li>');
						var cloneTwo = clone_element.clone().prop('id', 'exp_list_'+num );
						jQuery("#experience-list-container").append(cloneTwo);
						jQuery('#experience-list-container li:last span:first').attr("data-featherlight",'#work_'+num);
						jQuery('#experience-list-container li:last .employer').text(form.exp_employer);
						jQuery('#experience-list-container li:last .title').text(form.exp_title);
						jQuery('#experience-list-container li:last p').html(nl2br(form.exp_description));
						
						if(form.exp_fromDate)
							jQuery('#experience-list-container li:last .date').text(form.exp_fromDate + " - " + form.exp_toDate);
						else
							jQuery('#experience-list-container li:last .date').text(form.exp_fromDate);
						
						jQuery(".hide").append(clone);
						jQuery("#work_"+num+" [name=experience]").val(num);
						jQuery("#work_"+num+" [name=exp_employer]").val(form.exp_employer);
						jQuery("#work_"+num+" [name=exp_title]").val(form.exp_title);
						jQuery("#work_"+num+" [name=exp_toDate]").val(form.exp_toDate);
						jQuery("#work_"+num+" [name=exp_fromDate]").val(form.exp_fromDate);
						jQuery("#work_"+num+" [name=exp_description]").val(form.exp_description);
						jQuery(".hide #work_"+num+" [value=delete]").attr("onclick","delete_exp('"+num+"')");  
						jQuery(".hide #work_"+num+" [value=save]").attr("onclick","update_exp('"+num+"')"); 
						
						if(form.exp_stillWork)
							jQuery("#work_"+num+" [name=exp_stillWork]").prop('checked', true);
						
						jQuery("#emptyExp form").find("input, textarea").val("");
						jQuery(".hide #emptyExp [name=experience]").val(num + 1)
						
						if(jQuery("#error_submitting"))
							jQuery("#error_submitting").remove();
					
						jQuery.featherlight.close(function(){
							jQuery("#ajax_loading").remove();
							jQuery("#emptyExp form").show();
						});
						
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
							.appendTo(jQuery('#emptyExp form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#emptyExp form").slideDown("fast");
					}
				});
								
			}
			function emptyEdu(){
				event.preventDefault();
				
				var form = {
					action: "edu_update", 
					crud: "add",
					education_school: jQuery(".featherlight-content #emptyEducation form input[name=education_school]").val(),
					education_focus:  jQuery(".featherlight-content #emptyEducation form input[name=education_focus]").val(),
					attend_1:  jQuery(".featherlight-content #emptyEducation form [name=attend_1]").val(),
					attend_2:  jQuery(".featherlight-content #emptyEducation form [name=attend_2]").val(),
					education_degree:  jQuery(".featherlight-content #emptyEducation form input[name=education_degree]").val(),
					education: jQuery(".featherlight-content #emptyEducation form input[name=education]").val()
				};
								
				jQuery(".featherlight-content #emptyEducation form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #emptyEducation'))
						.slideDown("fast");
				});
			
				jQuery.post(ajaxurl, form, function(response) {
					if(response == "success") {
						
						var div = jQuery('div[id^="school_"]:last');
						
						if (div.size() == '1') {
							var num = parseInt( div.prop("id").match(/\d+/g), 10 ) +1;
							var clone = div.clone().prop('id', 'school_'+num );
						} else {
							var div = jQuery('#clone_school');
							var num = "0";
							var clone = div.clone().prop('id', 'school_'+num );
						}
						
						var clone_element = jQuery('<li id="edu_list_0" class="education"><span data-featherlight="#school_1" class="btn-edit btn-sumary">Edit</span><span class="title">rerara</span><br><span class="employer">32131</span><span class="focus">321315</span><span class="date">1900 - 1900</span></li>');
						var cloneTwo = clone_element.clone().prop('id', 'edu_list_'+num );
						jQuery("#education-list-container").append(cloneTwo);
						jQuery('#education-list-container li:last span:first').attr("data-featherlight",'#school_'+num);
						jQuery('#education-list-container li:last .focus').text(form.education_focus);
						jQuery('#education-list-container li:last .title').text(form.education_school);
						jQuery('#education-list-container li:last .employer').text(form.education_degree);
						
						if(form.attend_2)
							jQuery('#education-list-container li:last .date').text(form.attend_1 + " - " + form.attend_2);
						else
							jQuery('#education-list-container li:last .date').text(form.attend_1);
						
						jQuery(".hide").append(clone);
						jQuery("#school_"+num+" [name=education_school]").val(form.education_school);
						jQuery("#school_"+num+" [name=education_focus]").val(form.education_focus);
						jQuery("#school_"+num+" [value='"+form.attend_1+"']").prop('selected', true);
						jQuery("#school_"+num+" [value='"+form.attend_2+"']").prop('selected', true);
						jQuery("#school_"+num+" [name=education_degree]").val(form.education_degree);
						jQuery("#school_"+num+" [name=education]").val(form.education);
						jQuery(".hide #school_"+num+" [value=delete]").attr("onclick","delete_edu('"+num+"')");  
						jQuery(".hide #school_"+num+" [value=save]").attr("onclick","update_edu('"+num+"')"); 
									
						jQuery("#emptyEducation form").find("input, textarea").val("");
						jQuery(".hide #emptyEducation [name=education]").val(num + 1)
						
					
						if(jQuery("#error_submitting"))
							jQuery("#error_submitting").remove();
						
						jQuery.featherlight.close(function(){
							jQuery("#ajax_loading").remove();
							jQuery("#emptyEducation form").show();
						});
						
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
								.appendTo(jQuery('#emptyEducation form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#emptyEducation form").slideDown("fast");
					}
				});
			
			}
			function delete_exp(num){
				event.preventDefault();
				var form = {
					action: "exp_update", 
					crud: "delete",
					experience: jQuery(".featherlight-content #work_"+num+" form [name=experience]").val()
				};
				
				jQuery(".featherlight-content #work_"+num+" form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #work_'+num))
						.slideDown("fast");
				});
				
				jQuery.post(ajaxurl, form, function(response) {
					if(response == "success") {
						
						jQuery('#exp_list_'+num).slideUp("slow",function(){
							jQuery('#exp_list_'+num).remove();
							jQuery('.hide #work_'+num).remove();
							setTimeout(function(){
								var count_list = jQuery('[id^=exp_]').size();
	
								for(i = (num-1); i < count_list; ++i) {
									jQuery('#exp_list_'+(i+1)+' span:first').attr("data-featherlight",'#work_'+i);
									jQuery('#exp_list_'+(i+1)).attr("id",'exp_list_'+i);
									jQuery('.hide #work_'+(i+1)+' [name=experience]').attr("value",i);
									jQuery(".hide #work_"+(i+1)+" [value=delete]").attr("onclick","delete_exp('"+i+"')");  
									jQuery(".hide #work_"+(i+1)+" [value=save]").attr("onclick","update_exp('"+i+"')"); 
									jQuery('.hide #work_'+(i+1)).attr("id",'work_'+i);
								}

								jQuery.featherlight.close(function(){
									jQuery("#ajax_loading").remove();
								});
								
							},500);
						});
						
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
							.appendTo(jQuery('#work_'+num+' form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#work_"+num+" form").slideDown("fast");
					}
				});
			}
			function delete_edu(num){
				event.preventDefault();
				var form = {
					action: "edu_update", 
					crud: "delete",
					education: jQuery(".featherlight-content #school_"+num+" form [name=education]").val()
				};
				
				jQuery(".featherlight-content #school_"+num+" form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #school_'+num))
						.slideDown("fast");
				});
				
				jQuery.post(ajaxurl, form, function(response) {
					if(response == "success") {
						
						jQuery('#edu_list_'+num).slideUp("slow",function(){
							jQuery('#edu_list_'+num).remove();
							jQuery('.hide #school_'+num).remove();
							setTimeout(function(){
								var count_list = jQuery('[id^=exp_]').size();
	
								for(i = (num-1); i < count_list; ++i) {
									jQuery('#edu_list_'+(i+1)+' span:first').attr("data-featherlight",'#school_'+i);
									jQuery('#edu_list_'+(i+1)).attr("id",'edu_list_'+i);
									jQuery('.hide #school_'+(i+1)+' [name=education]').attr("value",i);
									jQuery(".hide #school_"+(i+1)+" [value=delete]").attr("onclick","delete_edu('"+i+"')");  
									jQuery(".hide #school_"+(i+1)+" [value=save]").attr("onclick","update_edu('"+i+"')"); 
									jQuery('.hide #school_'+(i+1)).attr("id",'school_'+i);
								}

								jQuery.featherlight.close(function(){
									jQuery("#ajax_loading").remove();
								});
								
							},500);
						});
						
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
							.appendTo(jQuery('#school_'+num+' form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#school_"+num+" form").slideDown("fast");
					}
				});
			}
			function update_exp(num){
				event.preventDefault();
				
				var checked;
				if(jQuery('.featherlight-content #work_' + num + ' form input[name=exp_stillWork]').is(':checked')) {
					checked = "1";
				} else {
					checked = "0";
				}
				
				var form = {
					action: "exp_update", 
					crud: "save",
					exp_employer: jQuery(".featherlight-content #work_"+num+" form input[name=exp_employer]").val(),
					exp_title:  jQuery(".featherlight-content #work_"+num+" form input[name=exp_title]").val(),
					exp_toDate:  jQuery(".featherlight-content #work_"+num+" form input[name=exp_toDate]").val(),
					exp_fromDate:  jQuery(".featherlight-content #work_"+num+" form input[name=exp_fromDate]").val(),
					exp_stillWork:  checked,
					exp_description: jQuery(".featherlight-content #work_"+num+" form [name=exp_description]").val(),
					experience: jQuery(".featherlight-content #work_"+num+" form input[name=experience]").val()
				};
				
				jQuery(".featherlight-content #work_"+num+" form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #work_'+num))
						.slideDown("fast");
				});
				
				jQuery.post(ajaxurl, form, function(response) {

					if(response == "success") {
						
						jQuery('#exp_list_'+num+' span:first').attr("data-featherlight",'#work_'+num);
						jQuery('#exp_list_'+num+' .employer').text(form.exp_employer);
						jQuery('#exp_list_'+num+' .title').text(form.exp_title);
						jQuery('#exp_list_'+num+' p').html(nl2br(form.exp_description));
						
						if(form.exp_fromDate)
							jQuery('#exp_list_'+num+' .date').text(form.exp_fromDate + " - " + form.exp_toDate);
						else
							jQuery('#exp_list_'+num+' .date').text(form.exp_fromDate);
						
						jQuery(".hide #work_"+num+" [name=exp_employer]").val(form.exp_employer);
						jQuery(".hide #work_"+num+" [name=exp_title]").val(form.exp_title);
						jQuery(".hide #work_"+num+" [name=exp_toDate]").val(form.exp_toDate);
						jQuery(".hide #work_"+num+" [name=exp_fromDate]").val(form.exp_fromDate);
						jQuery(".hide #work_"+num+" [name=exp_description]").val(form.exp_description);
						
						if(form.exp_stillWork)
							jQuery(".hide #work_"+num+" [name=exp_stillWork]").prop('checked', true);						
					
						jQuery.featherlight.close(function(){
							jQuery("#ajax_loading").remove();
							jQuery("#work_"+num+" form").show();
						});
						
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
							.appendTo(jQuery('#work_'+num+' form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#work_"+num+" form").slideDown("fast");
					}
				});
			}
			
			function update_edu(arg){
				event.preventDefault();

				var form = {
					action: "edu_update", 
					crud: "save",
					education_school: jQuery(".featherlight-content #school_" + arg + " form input[name=education_school]").val(),
					education_focus:  jQuery(".featherlight-content #school_" + arg + " form input[name=education_focus]").val(),
					attend_1:  jQuery(".featherlight-content #school_" + arg + " form [name=attend_1]").val(),
					attend_2:  jQuery(".featherlight-content #school_" + arg + " form [name=attend_2]").val(),
					education_degree:  jQuery(".featherlight-content #school_" + arg + " form input[name=education_degree]").val(),
					education: jQuery(".featherlight-content #school_" + arg + " form input[name=education]").val()
				};
				
				jQuery(".featherlight-content #school_"+arg+" form ").slideUp("normal",function(){
					jQuery("<img style='margin: 0 auto;' id='ajax_loading' src='/wp-content/themes/freelanceengine/includes/aecore/assets/img/loading.gif' />")
						.appendTo(jQuery('.featherlight-content #school_'+arg))
						.slideDown("fast");
				});

				jQuery.post(ajaxurl, form, function(response) {

					if(response == "success") {
	
						jQuery("#edu_list_"+arg+" .focus").text(form.education_focus);
						jQuery("#edu_list_"+arg+" .title").text(form.education_school);
						jQuery("#edu_list_"+arg+" .employer").text(form.education_degree);
						
						if(form.attend_2)
							jQuery("#edu_list_"+arg+" .date").text(form.attend_1 + " - " + form.attend_2);
						else
							jQuery("#edu_list_"+arg+" .date").text(form.attend_1);

						jQuery(".hide #school_"+arg+" [name=education_school]").val(form.education_school);
						jQuery(".hide #school_"+arg+" [name=education_focus]").val(form.education_focus);
						
						
						jQuery(".hide #school_"+arg+" [name=attend_1]").empty();
						jQuery(".hide #school_"+arg+" [name=attend_2]").empty();
						
						<?php $year_loop = date("Y"); ?>
						for(i = 1900; i <= <?php echo $year_loop; ?>; ++i ) {
							if(!form.attend_1) form.attend_1 = new Date().getFullYear();
							if(i == form.attend_1)
								jQuery(".hide #school_"+arg+" [name=attend_1]").append("<option selected value='"+i+"'>"+i+"</option>");
							else
								jQuery(".hide #school_"+arg+" [name=attend_1]").append("<option value='"+i+"'>"+i+"</option>");
						}
						for(i = 1900; i <= <?php echo $year_loop+7; ?>; ++i ) {
							if(!form.attend_2) form.attend_2 = new Date().getFullYear();
							if(i == form.attend_2)
								jQuery(".hide #school_"+arg+" [name=attend_2]").append("<option selected value='"+i+"'>"+i+"</option>");
							else
								jQuery(".hide #school_"+arg+" [name=attend_2]").append("<option value='"+i+"'>"+i+"</option>");
						}
						
						if(jQuery("#error_submitting"))
							jQuery("#error_submitting").remove();
					
						jQuery("#school_"+arg+" [name=education_degree]").val(form.education_degree);
						jQuery("#school_"+arg+" [name=education]").val(form.education);
						
						jQuery.featherlight.close(function(){
							jQuery("#ajax_loading").remove();
							jQuery("#school_"+arg+" form").show();
						});
					
					} else {
						if(!jQuery("#error_submitting"))
							jQuery("<span id='error_submitting' style='color: red;'>There was an error submitting.</span>")
								.appendTo(jQuery('#school_'+arg+' form'));
						jQuery("#ajax_loading").slideUp(500);
						jQuery("#school_"+arg+" form").slideDown("fast");
					}
				});
			
			}
		</script>
	</div>
	<div id="emptyEducation">
		<form style="width: 420px;" class="form-horizontal" method="post">
		<fieldset>
		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="education_school">School Name</label>  
		  <div class="col-md-8">
		  <input name="education_school" type="text" placeholder="What school did you go to?" class="form-control input-md" required="">
			
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="education_focus">Major</label>  
		  <div class="col-md-8">
		  <input name="education_focus" type="text" placeholder="What did you focus on?" class="form-control input-md">
			
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="education_degree">Degree</label>  
		  <div class="col-md-8">
			<input name="education_degree" type="text" placeholder="What degree did you earn?" class="form-control input-md">
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="education_year">Attended Date</label>  
		  <div class="col-md-8">
			  <select name="attend_1">
			  <?php unset($date); ?>
			  <?php $year_loop = date("Y"); ?>
			  <?php for($yeari = 1900; $yeari <= $year_loop; $yeari++) {
					if(date("Y") == $yeari)
						$date =  "<option selected value='".$yeari."'>".$yeari."</option>".$date;
					else
						$date = "<option value='".$yeari."'>".$yeari."</option>".$date;
			  } 
			  echo $date;?>
			  </select>
			  <span> - </span>
			  <select name="attend_2">
			  <?php unset($date); ?>
			  <?php for($yeari = 1900; $yeari <= ($year_loop+7); $yeari++) {
					if(date("Y") == $yeari)
						$date =  "<option selected value='".$yeari."'>".$yeari."</option>".$date;
					else
						$date = "<option value='".$yeari."'>".$yeari."</option>".$date;
			  } 
			  echo $date;?>
			  </select>
		  </div>
		</div>

		<!-- Button -->
		<div class="form-group text-right">
		  <div class="col-md-12">
			<button type="button" onclick="jQuery.featherlight.close()" name="close" class="btn btn-cancel" value="cancel" name="action">Cancel</button>
			<button type="button" onclick="emptyEdu()" name="action" value="add" class="btn btn-primary">Add</button>
		  </div>
		</div>

		<input type="hidden" value="1" name="education" />
		
		</fieldset>
		</form>

	</div>
	<div id="warningPDF" style="max-width: 400px;">
		<p>
		 Note that any information you choose to sync from your PDF will 
		 overwrite anything you already have in the corresponding section 
		 of your Skillquo profile.
		</p>
		<div class="form-group text-right">
		  <div class="col-md-12">
			<button type="button" onclick="jQuery.featherlight.close();" name="singlebutton" class="btn btn-danger">Cancel</button>
			<button type="button" onclick="jQuery('#fileToUpload').click();" name="singlebutton" class="btn btn-success">Continue</button>
		  </div>
		</div>
	</div>
<?php } ?>
</div>
<?php } ?>
<?php if(!$under_review) { ?>
<div class="hide">
	<div id="warningReview" style="max-width: 400px;">
	<?php if($status == 2) { ?>
		<p>Resubmitting again?</p>
		<p>You got one more chance, and if you are rejected again, <br/>you are out!</p>
		<p>Make sure your profile is perfect!</<p>
		<p>Are you ready?</p>
	<?php } else { ?>
		<p>
		 Hello! You have finally made it to the final stages! 
		 But, be warned! Once you have submit your profile for review,
		 You can no longer edit it. Are you ready?!
		</p>
	<?php } ?>
		<div class="form-group text-right">
		  <div class="col-md-12">
			<button type="button" onclick="jQuery.featherlight.close();" name="singlebutton" class="btn btn-danger">Wait, I need to check again...</button>
			<button id="the_final_countdown" name="singlebutton" class="btn btn-success">Yes, Let's go!</button>
			<script>
				jQuery("#the_final_countdown").click( function () {
					jQuery(".featherlight-content #warningReview").slideUp(500,function(){
						jQuery(".featherlight-content #warningReview").html("<img src='/wp-content/themes/freelanceengine/includes/aecore/assets/img//loading.gif' />").slideDown(1000);
					});
					document.getElementById('submitForReview_form').submit();
				});
			</script>
		  </div>
		</div>
	</div>
	<div id="warningBanner" style="max-width: 400px;">
		<p>
		 Please save all your setting before uploading a Banner as the page will reload when you do.
		</p>
		<div class="form-group text-right">
		  <div class="col-md-12">
			<button type="button" onclick="jQuery.featherlight.close();" name="singlebutton" class="btn btn-danger">Cancel</button>
			<button onclick="jQuery('#user_banner').click();" name="singlebutton" class="btn btn-success">Continue</button>
		  </div>
		</div>
	</div>
</div>
<?php } ?>

<?php
	get_footer();
?>

