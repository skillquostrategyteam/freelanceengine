<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */


global $wp_query, $ae_post_factory, $post, $user_ID;
$post_object = $ae_post_factory->get(PROJECT);
$convert = $post_object->convert($post);

/*
if($post->ID) {

	if($_POST["skillquo_approve"] && current_user_can( "activate_plugins" )) {
		$value = 0;
		$rejected_email = get_the_author_meta( 'user_email', $author_id);
		if($_POST["skillquo_approve"] == 1) {
			$value = 1;
			wp_mail($rejected_email,"Your Project has been approved!","Congratulation. We have approved your Project.");
		} else if($_POST["skillquo_approve"] == 2) { 
			$value = 2;
			
			$content .= "Unfortunately, your project was rejecting for the following reasons:"."\r\n"."\r\n";
			if($_POST["reject_1"]) $content .= "Inappropriate Project"."\r\n";
			if($_POST["reject_2"]) $content .= "Insufficient Content"."\r\n";
			if($_POST["reject_3"]) $content .= "Spam"."\r\n";					
			if($_POST["reject_4"]) $content .= "Others"."\r\n";

			$content .= "\r\n"."\r\n";
			$content .= sprintf("%s",$_POST["reason"])."\r\n";
			
			wp_mail($rejected_email,"Your Project has been Rejected",$content);
		} else {
			wp_mail($rejected_email,"Your Project is being reviewed!","Thank you for submitting your project! 
			Your project has been received and our team is in the process of reviewing it for completeness, accuracy and fit for SkillQuo. 
			We typically approve projects within 4 business days, but sometimes it takes longer due to additional reference checks. 
			We will keep you posted on the progress, and will let you know if we need any other information. 
			We wish you a fruitful partnership with SkillQuo. We take great care and pride in being a true community rather than - just a platform."."\r\n"."\r\n".
			"We encourage you to take advantage of the many resources at your disposal, from our blog site, our weekly subscription, 
			and stay tuned for our most exciting reveal of all: SkillQuo Cares forum. This site will serve our comprehensive support community for our SkillQuo consultants.".
			"\r\n"."\r\n"."We will provide everything from chat rooms, to white papers to featured consultant success stories. 
			We will update you on our offsite and onsite activities such as meet-and-greets to sweepstakes and contests. As we said, we stand by our word to you in being a true community.".
			"\r\n"."\r\n"."With SkillQuo, freelancing is associated with exclusivity, prestige, and the brightest minds solving the most intriguing, layered business problems.".
			"\r\n"."\r\n"."We hope you feel empowered to chart your own path, augment your income, and build your powerhouse brand."."\r\n"."\r\n".
			"Please feel free to reach out to us at anytime, with questions."."\r\n"."\r\n"."Sincerely,"."\r\n"."Arathi Dar, Co-Founder & CMO at SkillQuo");
		}
		update_post_meta($post->ID,"skillquo_approve",$value);
	}

	$approve = get_post_meta($post->ID,"skillquo_approve",true);

}

if(($approve != 1 && !current_user_can( "activate_plugins" ))) {
	header("Location: /");
}
*/

get_header();

if(have_posts()) { the_post(); 	// get breacrumb of single project
    get_template_part('template/single-project','breadcrumb' );
    ?>
    <div class="single-project-wrapper">
    	<div class="container">
        	<div class="row">
            	<?php get_template_part('template/project','detail' ); ?>
                <?php get_template_part('template/list','bids' ); ?>
            </div> <!-- end .row !-->
        </div>
    </div>
	<?php
    echo '<script type="data/json" id="project_data">'.json_encode($convert).'</script>';
}
get_footer();