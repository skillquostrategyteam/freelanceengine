<div class="modal fade" id="modal_login">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form role="form" id="signin_form" class="auth-form signin_form">
					<?php
						if( function_exists('ae_render_social_button')){
							$before_string = ""; //__("You can also sign in by:", ET_DOMAIN);
							ae_render_social_button( array(), array(), $before_string );
						}
					?>
					<p class="or"><span style="background: #fff; padding: 0 10px;">or</span></p>
					<div class="form-group">
						<input type="text" class="form-control" id="login_user_login" name="user_login" placeholder="<?php _e('Enter Username or Email', ET_DOMAIN) ?>">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="login_user_pass" name="user_pass" placeholder="<?php _e('Password', ET_DOMAIN) ?>">
					</div>
                    <div class="clearfix"></div>
					<button type="submit" class="btn-submit btn-sumary btn-sub-create">
						<?php _e('Sign in', ET_DOMAIN) ?>
					</button>
                    <a class="show-forgot-form" href="#"><?php _e("Forgot Password?", ET_DOMAIN) ?></a>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog login -->
</div><!-- /.modal -->
