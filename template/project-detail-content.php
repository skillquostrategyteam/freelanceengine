<?php
/**
 * The template for displaying project description, comment, taxonomy and custom fields
 * @since 1.0
 * @package FreelanceEngine
 * @category Template
 */
global $wp_query, $ae_post_factory, $post, $user_ID;
$post_object    = $ae_post_factory->get(PROJECT);
$convert = $project = $post_object->current_post;
$approve = get_post_meta($post->ID,"skillquo_approve",true);
?>
<div class="info-project-item-details">
    <div class="row">
        <div class="col-md-8">
            <div class="content-require-project">
                <h4><?php _e('Project description:',ET_DOMAIN);?></h4>
                <?php the_content(); ?>
            </div>
			<?php if($require_experience = get_post_meta($post->ID,"required_experience",true)) { ?>
			<div class="content-require-project">
				<h4><?php _e('Required Experience:',SQ_DOMAIN);?></h4>
				<?php echo $require_experience; ?>
			</div>
			<?php } ?>
			<?php if($idealCan = get_post_meta($post->ID,"ideal_profession",true)) { ?>
			<div class="content-require-project">
				<h4><?php _e('Ideal Candidate:',SQ_DOMAIN);?></h4>
				<?php echo $idealCan; ?>
				<div>
					<?php
						$weekDelivery = get_post_meta($post->ID,"estimate_duration",true);
						if($weekDelivery >= 1) echo "<strong>Delivery Expectation:</strong> ".$weekDelivery." weeks <br/>";

						$requireYear = get_post_meta($post->ID,"required_years",true);
						if($requireYear >= 1) echo "<strong>Require Years of Experience:</strong> ".$requireYear;
					?>
				</div>
			</div>
			<?php } ?>
            <?php if(!ae_get_option('disable_project_comment')) { ?>
            <div class="comments" id="project_comment">
                <?php comments_template('/comments.php', true)?>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <div class="content-require-skill-project">
			
            <?php

                do_action('before_sidebar_single_project', $project);

                list_tax_of_project( get_the_ID(), __('Skills required:',ET_DOMAIN), 'skill' );
                list_tax_of_project( get_the_ID(), __('Category:',ET_DOMAIN)  );

                // list project attachment
                $attachment = get_children( array(
                        'numberposts' => -1,
                        'order' => 'ASC',
                        'post_parent' => $post->ID,
                        'post_type' => 'attachment'
                      ), OBJECT );
                if(!empty($attachment)) {
                    echo '<h3 class="title-content">'. __("Attachments:", ET_DOMAIN) .'</h3>';
                    echo '<ul class="list-file-attack-report">';
                    foreach ($attachment as $key => $att) {
                        $file_type = wp_check_filetype($att->post_title, array('jpg' => 'image/jpeg',
                                                                                'jpeg' => 'image/jpeg',
                                                                                'gif' => 'image/gif',
                                                                                'png' => 'image/png',
                                                                                'bmp' => 'image/bmp'
                                                                            )
                                                    );
                        $class="text-ellipsis";
                        if(isset($file_type['ext']) && $file_type['ext']) $class="image-gallery text-ellipsis";
                        echo '<li>
                                <a class="'.$class.'" target="_blank" href="'.$att->guid.'"><i class="fa fa-paperclip"></i>'.$att->post_title.'</a>
                            </li>';
                    }
                    echo '</ul>';
                }
                if(function_exists('et_render_custom_field')) {
                    et_render_custom_field($project);
                }

                do_action('after_sidebar_single_project', $project);
            ?>
            </div>
			<?php $under_review = get_user_meta($post->post_author,"under_review",true);?>
			
			<?php if(current_user_can( "activate_plugins" ) && $post->ID && $under_review && $hide){ ?>
			<div class="info-company-wrapper">
				<div class="row">
					<div class="col-md-12">
						<ul class="list-info-company-details">
							<li>
								<div class="hired"><i class="fa fa-smile-o"></i>
									Status: 
										<?php if($approve == 1) { ?>
											<span style="color: #8BC34A;" class="info" title="">Approve</span>
										<?php } else if($approve == 2){?>
											<span style="color: #FF5252;" class="info" title="">Rejected</span>
										<?php } else { ?>
											<span  style="color: #2196F3;" class="info" title="">In Review</span>
										<?php } ?>
								</div>
							</li>
							<li style="text-align: right;">
								<form method="POST">
									<button value="1" name="skillquo_approve">Approve</button>
									<button value="3" name="skillquo_approve">Re-review</button>
									<button data-featherlight="#whyReject" onclick="preventDefault();">Reject</button>
								</form>
								
							</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="hide">
				<div id="whyReject">
					<form method="POST">
						<h3 style="margin: 0 0 20px 0;">Why are you rejecting?</h3>
						<input type="checkbox" name="reject_1" value="1"> Inappropriate Project
						<br/><input type="checkbox" name="reject_2" value="1"> Insufficient Content
						<br/><input type="checkbox" name="reject_3" value="1"> Spam
						<br/><input type="checkbox" name="reject_4" value="1"> Others
						<br/><textarea rows="4" style="width: 100%; min-height: 100px;" name="reason"></textarea>
						<br/><button type="submit" name="skillquo_approve" value="2">Reject</button>
					</form>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>