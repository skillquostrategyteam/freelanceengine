<?php  
header("Access-Control-Allow-Origin: *");
include 'wp-config.php';
	global $wpdb, $wp_hasher;
    $ae_users = AE_Users::get_instance();
	$mail = AE_Mailing::get_instance();
	$action=$_POST['action'];	
	switch($action){
		case 'CheckEmailForPassword':

            $response = array('message'=>'recieved');
			
            $post_email = sprintf("%s",$_POST['useremail']);

            $errors = new WP_Error();

			if ( empty( $post_email ) ) {
				$errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));
			} elseif ( strpos( $post_email, '@' ) ) {
                $user_data = get_user_by( 'email', trim( wp_unslash( $post_email ) ) );
                if ( empty( $user_data ) )
                    $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
            } else {
                $login = trim($post_email);
                $user_data = get_user_by('login', $login);
            }
			
            do_action( 'lostpassword_post', $errors );

            // Redefining user_login ensures we return the right case in the email.
            $user_login = $user_data->user_login;
            $user_email = $user_data->user_email;
            $key = get_password_reset_key( $user_data );

            if ( is_wp_error( $key ) ) {
                return $key;
            }

            if($errors) $response = $errors;

            $message = __('Someone has requested a password reset for the following account:') . "\r\n\r\n";
            $message .= network_home_url( '/' ) . "\r\n\r\n";
            $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
            $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
            $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
            $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

            if ( is_multisite() )
                $blogname = $GLOBALS['current_site']->site_name;
            else
                $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

            $title = sprintf( __('[%s] Password Reset'), $blogname );

            $title = apply_filters( 'retrieve_password_title', $title, $user_login, $user_data );

            $message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );

            if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
                $response->error =  "The email could not be sent <br />\n Possible reason: your host may have disabled the mail() function";
            
			wp_send_json( $response );
		break;		
		case 'CompanySignUp':
			$emailexits=email_exists($_POST['email']);
			if($emailexits){									
				$response=array('valid'=>false,'email'=>'Email Address Already Exist!','username'=>'');	
			} else {
				$newusername=create_unique_user(strtolower($_POST['fname'].$_POST['lname']));
				$userdata=array(
				    'user_pass'=>$_POST['password'],
                    'user_email'=>$_POST['email'],
                    'user_login'=>$newusername,
                    'user_nicename'=>$_POST['lname'],
                    'description'=>'',
                    'nickname'=>$_POST['fname'],
                    'display_name'=>$_POST['fname']." ".$_POST['lname'],
                    'first_name'=>$_POST['fname'],
                    'last_name'=>$_POST['lname'],
                    'role'=>'employer');

				$result = $ae_users->insert( $userdata );

				if(!is_wp_error($result->ID)){
					add_user_meta($result->ID, 'company_name', $_POST['company']);
					add_user_meta($result->ID, 'country_name', $_POST['country']);									
					$response=array('valid'=>true,'username'=>$newusername,'auth'=>encypassword($_POST['password']),'role'=>"employer");
				}
			}
		wp_send_json( $response );
		break;
		case 'IndividualSignUp':
			$emailexits=email_exists($_POST['email']);
			if($emailexits){									
				$response=array('valid'=>false,'email'=>'Email Address Already Exist!','username'=>'');	
			} else {
				$newusername=create_unique_user(strtolower($_POST['fname'].$_POST['lname']));
				$userdata=array('user_pass'=>$_POST['password'],'user_email'=>$_POST['email'],'user_login'=>$newusername,'user_nicename'=>$_POST['lname'],'description'=>'','nickname'=>$_POST['fname'],'display_name'=>$_POST['fname']." ".$_POST['lname'],'first_name'=>$_POST['fname'],'last_name'=>$_POST['lname'],'role'=>'freelancer');
                $result = $ae_users->insert( $userdata );

				if(!is_wp_error($result->ID)){

					$username=getusername($_POST['email']);
					
					$creds= array(
						'user_login'    => $username,
						'user_password' => $_POST['password']
					);

					$userlogin=wp_signon( $creds, false );
					if(!is_wp_error($userlogin)) {
						$response=array('valid'=>true,'username'=>$username,'auth'=>encypassword($_POST['password']),'role'=>"freelancer");
					} else {
						$response=array('valid'=>false,'username'=>$creds['user_login'], 'error' => is_wp_error($userlogin), 'username' => $username, 'login' => $userlogin, 'result' => $result);
					}
					
				}
			}
		wp_send_json( $response );
		break;
		case 'UserLogin':

		  $username=getusername($_POST['user_login']);
		  $password=$_POST['user_pass'];
		  $creds= array(
				'user_login'    => $username,
				'user_password' => $password
			);
		  $userlogin=wp_signon( $creds, false );
		  if(!is_wp_error($userlogin)){
		  		$user_info = get_userdata($userlogin->ID);
      				$userrole=implode(', ', $user_info->roles);
		  		$response=array('valid'=>true,'username'=>$creds['user_login'],'auth'=>encypassword($creds['user_password']),'role'=>$userrole);
		  }
		 else
		 	$response=array('valid'=>false,'username'=>$creds['user_login']);
		wp_send_json( $response );
		break;		
	}
function encypassword($password){
	$newpassword='';
	for($i=0;$i<strlen($password);$i++){
		$newpassword.=$password[$i].randomkey();
	}
	return $newpassword;
}
function encypassword12($key){
	$newpassword='';
	for($i=0;$i<strlen($key);$i++){
		$newpassword.=$key[$i].randomkey1();
	}
	return $newpassword;
}
function randomkey(){
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 2; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
}
function randomkey1(){
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 1; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
}
function getusername($username) {
  if ( ! empty( $username ) && is_email( $username ) ) :
	if ( $user = get_user_by('email', $username) )
	  $username = $user->user_login;
  endif;
	return $username;
  
}
/*------------------------------------------------ Generate Slug ----------------------------------------------------*/
	function Slug($string)
	{
			return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), ''));
	}
	function check_exists_user($slug)
	{
		if(username_exists($slug))	
			return true;
		else
			return false;
			
	}	
	function create_unique_user($val,$counter=0)
	{
		if($counter>0)		
			$slug = Slug($val).$counter;		
		else		
			$slug = Slug($val);		
		$check_user_exists =check_exists_user($slug);
		if($check_user_exists)
		{
			$counter++;
			 return create_unique_user($val,$counter);
			
		}
		return $slug;
		
	}
	

//header("Access-Control-Allow-Origin: *");



?>
